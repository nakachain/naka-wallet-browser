const assert = require('assert')
const { getTxLink } = require('../../../ui/lib/explorer-link')

describe('Explorer Network Prefix', () => {
  it('returns empy string as default value', () => {
    assert.equal(getTxLink(undefined, 'abc'), '')
  })

  it('returns the link for networkId of 25', () => {
    assert.equal(getTxLink(25, 'abc'), 'http://testnet.nakachain.org/tx/abc')
  })

  it('returns the link for networkId of 2019', () => {
    assert.equal(getTxLink(2019, 'abc'), 'http://explorer.nakachain.org/tx/abc')
  })
})
