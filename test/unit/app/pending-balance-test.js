const assert = require('assert')
const PendingBalanceCalculator = require('../../../app/scripts/lib/pending-balance-calculator')
const MockTxGen = require('../../lib/mock-tx-gen')
const BN = require('ethereumjs-util').BN

const zeroBn = new BN(0)
const nakaBn = new BN(String(1e18))
const naka = '0x' + nakaBn.toString(16)

describe('PendingBalanceCalculator', function () {
  let balanceCalculator, pendingTxs

  describe('#calculateMaxCost(tx)', function () {
    it('returns a BN for a given tx value', function () {
      const txGen = new MockTxGen()
      pendingTxs = txGen.generate({
        status: 'submitted',
        txParams: {
          value: naka,
          gasPrice: '0x0',
          gas: '0x0',
        },
      }, { count: 1 })

      const balanceCalculator = generateBalanceCalcWith([], zeroBn)
      const result = balanceCalculator.calculateMaxCost(pendingTxs[0])
      assert.equal(result.toString(), nakaBn.toString(), 'computes one Naka')
    })

    it('calculates gas costs as well', function () {
      const txGen = new MockTxGen()
      pendingTxs = txGen.generate({
        status: 'submitted',
        txParams: {
          value: '0x0',
          gasPrice: '0x2',
          gas: '0x3',
        },
      }, { count: 1 })

      const balanceCalculator = generateBalanceCalcWith([], zeroBn)
      const result = balanceCalculator.calculateMaxCost(pendingTxs[0])
      assert.equal(result.toString(), '6', 'computes 6 wei of gas')
    })
  })

  describe('if you have no pending txs and one Naka', function () {

    beforeEach(function () {
      balanceCalculator = generateBalanceCalcWith([], nakaBn)
    })

    it('returns the network balance', async function () {
      const result = await balanceCalculator.getBalance()
      assert.equal(result, naka, `gave ${result} needed ${naka}`)
    })
  })

  describe('if you have a one Naka pending tx and one Naka', function () {
    beforeEach(function () {
      const txGen = new MockTxGen()
      pendingTxs = txGen.generate({
        status: 'submitted',
        txParams: {
          value: naka,
          gasPrice: '0x0',
          gas: '0x0',
        },
      }, { count: 1 })

      balanceCalculator = generateBalanceCalcWith(pendingTxs, nakaBn)
    })

    it('returns the subtracted result', async function () {
      const result = await balanceCalculator.getBalance()
      assert.equal(result, '0x0', `gave ${result} needed '0x0'`)
      return true
    })

  })
})

function generateBalanceCalcWith (transactions, providerStub = zeroBn) {
  const getPendingTransactions = async () => transactions
  const getBalance = async () => providerStub

  return new PendingBalanceCalculator({
    getBalance,
    getPendingTransactions,
  })
}

