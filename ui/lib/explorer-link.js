const { getNetworkById } = require('../../config/network')

const getAccountLink = (networkId, address) => {
  const network = getNetworkById(parseInt(networkId))
  if (network) {
    return `${network.explorerHost}/address/${address}`
  }
  return ''
}

const getTxLink = (networkId, txid) => {
  const network = getNetworkById(parseInt(networkId))
  if (network) {
    return `${network.explorerHost}/tx/${txid}`
  }
  return ''
}

const getTokenLink = (networkId, address) => {
  const network = getNetworkById(parseInt(networkId))
  if (network) {
    return `${network.explorerHost}/tokens/${address}`
  }
  return ''
}

module.exports = {
  getAccountLink,
  getTxLink,
  getTokenLink,
}
