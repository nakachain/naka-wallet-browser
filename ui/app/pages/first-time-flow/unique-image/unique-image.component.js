import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Identicon from '../../../components/identicon'
import { INITIALIZE_NOTICE_ROUTE } from '../../../routes'

export default class UniqueImageScreen extends Component {
  static propTypes = {
    address: PropTypes.string,
    history: PropTypes.object,
  }

  render () {
    return (
      <div className="first-view-main-wrapper">
        <div className="first-view-main first-view-main__center">
          <div className="unique-image first-time-flow__main-container">
            <Identicon address={this.props.address} diameter={60} />
            <div className="unique-image__title">Your unique account image</div>
            <div className="unique-image__body-text">
              This image was programmatically generated for you by your new account number.
            </div>
            <div className="unique-image__body-text">
              You’ll see this image everytime you need to confirm a transaction.
            </div>
            <button
              className="first-time-flow__button"
              onClick={() => this.props.history.push(INITIALIZE_NOTICE_ROUTE)}
            >
              Next
            </button>
          </div>
        </div>
      </div>
    )
  }
}
