import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose } from 'recompose'
import UniqueImageScreen from './unique-image.component'

function mapStateToProps (state) {
  const {
    metamask: {
      selectedAddress,
    },
  } = state

  return {
    address: selectedAddress,
  }
}

module.exports = compose(
  withRouter,
  connect(mapStateToProps)
)(UniqueImageScreen)
