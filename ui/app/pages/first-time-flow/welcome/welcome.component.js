import EventEmitter from 'events'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Mascot from '../../../components/mascot'
import { INITIALIZE_CREATE_PASSWORD_ROUTE } from '../../../routes'

export default class WelcomeScreen extends Component {
  static propTypes = {
    closeWelcomeScreen: PropTypes.func.isRequired,
    welcomeScreenSeen: PropTypes.bool,
    history: PropTypes.object,
    t: PropTypes.func,
  }

  static contextTypes = {
    t: PropTypes.func,
  }

  constructor (props) {
    super(props)
    this.animationEventEmitter = new EventEmitter()
  }

  componentWillMount () {
    const { history, welcomeScreenSeen } = this.props

    if (welcomeScreenSeen) {
      history.push(INITIALIZE_CREATE_PASSWORD_ROUTE)
    }
  }

  initiateAccountCreation = () => {
    this.props.closeWelcomeScreen()
    this.props.history.push(INITIALIZE_CREATE_PASSWORD_ROUTE)
  }

  render () {
    const { t } = this.context
    return (
      <div className="welcome-screen">
        <div className="welcome-screen__info">
          <Mascot />
          <div className="welcome-screen__info__header">{t('nakaWalletDescription')}</div>
          <div className="welcome-screen__info__copy">{t('walletDescription')}</div>
          <button className="welcome-screen__button" onClick={this.initiateAccountCreation}>
            {t('continue')}
          </button>
        </div>
      </div>
    )
  }
}
