import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose } from 'recompose'
import WelcomeScreen from './welcome.component'
import { closeWelcomeScreen } from '../../../actions'

function mapStateToProps (state) {
  const {
    metamask: {
      welcomeScreenSeen,
    },
  } = state

  return {
    welcomeScreenSeen,
  }
}

function mapDispatchToProps (dispatch) {
  return {
    closeWelcomeScreen: () => dispatch(closeWelcomeScreen()),
  }
}

module.exports = compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(WelcomeScreen)
