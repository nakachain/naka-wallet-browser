import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose } from 'recompose'
import ImportAccountScreen from './import-account.component'
import { importNewAccount, hideWarning } from '../../../actions'

function mapStateToProps (state) {
  const {
    appState: {
      isLoading,
      warning,
    },
  } = state

  return {
    isLoading,
    warning,
  }
}

function mapDispatchToProps (dispatch) {
  return {
    importNewAccount: (strategy, args) => dispatch(importNewAccount(strategy, args)),
    hideWarning: () => dispatch(hideWarning()),
  }
}

module.exports = compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(ImportAccountScreen)
