import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose } from 'recompose'
import CreatePasswordScreen from './create-password.component'
import { createNewVaultAndKeychain } from '../../../actions'

function mapStateToProps (state) {
  const {
    metamask: {
      isInitialized,
      isUnlocked,
      noActiveNotices,
    },
    appState: {
      isLoading,
    },
  } = state

  return {
    isLoading,
    isInitialized,
    isUnlocked,
    noActiveNotices,
  }
}

function mapDispatchToProps (dispatch) {
  return {
    createAccount: password => dispatch(createNewVaultAndKeychain(password)),
  }
}

module.exports = compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(CreatePasswordScreen)
