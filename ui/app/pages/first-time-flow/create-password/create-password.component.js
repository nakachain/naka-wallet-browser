import React, { Component } from 'react'
import PropTypes from 'prop-types'
import EventEmitter from 'events'
import {
  INITIALIZE_UNIQUE_IMAGE_ROUTE,
  INITIALIZE_IMPORT_WITH_SEED_PHRASE_ROUTE,
  INITIALIZE_NOTICE_ROUTE,
} from '../../../routes'
import TextField from '../../../components/text-field'

export default class CreatePasswordScreen extends Component {
  static contextTypes = {
    t: PropTypes.func,
  }

  static propTypes = {
    isLoading: PropTypes.bool.isRequired,
    createAccount: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired,
    isInitialized: PropTypes.bool,
    isUnlocked: PropTypes.bool,
  }

  state = {
    password: '',
    confirmPassword: '',
    passwordError: null,
    confirmPasswordError: null,
  }

  constructor (props) {
    super(props)
    this.animationEventEmitter = new EventEmitter()
  }

  componentWillMount () {
    const { isInitialized, history } = this.props

    if (isInitialized) {
      history.push(INITIALIZE_NOTICE_ROUTE)
    }
  }

  isValid () {
    const { password, confirmPassword } = this.state

    if (!password || !confirmPassword) {
      return false
    }

    if (password.length < 8) {
      return false
    }

    return password === confirmPassword
  }

  createAccount = (event) => {
    event.preventDefault()

    if (!this.isValid()) {
      return
    }

    const { password } = this.state
    const { createAccount, history } = this.props

    this.setState({ isLoading: true })
    createAccount(password)
      .then(() => history.push(INITIALIZE_UNIQUE_IMAGE_ROUTE))
  }

  handlePasswordChange (password) {
    const { confirmPassword } = this.state
    let confirmPasswordError = null
    let passwordError = null

    if (password && password.length < 8) {
      passwordError = this.context.t('passwordNotLongEnough')
    }

    if (confirmPassword && password !== confirmPassword) {
      confirmPasswordError = this.context.t('passwordsDontMatch')
    }

    this.setState({ password, passwordError, confirmPasswordError })
  }

  handleConfirmPasswordChange (confirmPassword) {
    const { password } = this.state
    let confirmPasswordError = null

    if (password !== confirmPassword) {
      confirmPasswordError = this.context.t('passwordsDontMatch')
    }

    this.setState({ confirmPassword, confirmPasswordError })
  }

  handleCreateButtonClick = (event) => {
    event.preventDefault()
    this.props.history.push(INITIALIZE_IMPORT_WITH_SEED_PHRASE_ROUTE)
  }

  render () {
    const { passwordError, confirmPasswordError } = this.state
    const { t } = this.context

    return (
      <div className="first-view-main-wrapper">
        <div className="first-view-main first-view-main__center">
          <div className="first-time-flow__main-container">
            <form className="create-password">
              <div className="create-password__title">
                Create Password
              </div>
              <TextField
                id="create-password"
                className="first-time-flow__input"
                label={t('newPassword')}
                type="password"
                value={this.state.password}
                onChange={event => this.handlePasswordChange(event.target.value)}
                error={passwordError}
                autoFocus
                autoComplete="new-password"
                margin="normal"
                fullWidth
                largeLabel
              />
              <TextField
                id="confirm-password"
                className="first-time-flow__input"
                label={t('confirmPassword')}
                type="password"
                value={this.state.confirmPassword}
                onChange={event => this.handleConfirmPasswordChange(event.target.value)}
                error={confirmPasswordError}
                autoComplete="confirm-password"
                margin="normal"
                fullWidth
                largeLabel
              />
            </form>
            <button
              className="first-time-flow__button"
              disabled={!this.isValid()}
              onClick={this.createAccount}
            >
              Create
            </button>
            <div className="create-password__import-button-container">
              <a
                href=""
                className="first-time-flow__link"
                onClick={this.handleCreateButtonClick}
              >
                Import with seed phrase
              </a>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
