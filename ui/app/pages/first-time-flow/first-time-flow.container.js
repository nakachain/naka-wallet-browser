import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { compose } from 'recompose'
import FirstTimeFlow from './first-time-flow.component'

const mapStateToProps = ({ metamask }) => {
  const {
    isInitialized,
    seedWords,
    noActiveNotices,
    selectedAddress,
    forgottenPassword,
    isUnlocked,
    welcomeScreenSeen,
    isPopup,
  } = metamask

  return {
    isInitialized,
    seedWords,
    noActiveNotices,
    address: selectedAddress,
    forgottenPassword,
    isUnlocked,
    welcomeScreenSeen,
    isPopup,
  }
}

export default compose(
  withRouter,
  connect(mapStateToProps)
)(FirstTimeFlow)
