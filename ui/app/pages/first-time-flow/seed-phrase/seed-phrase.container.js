import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose } from 'recompose'
import BackupPhraseScreen from './seed-phrase.component'

function mapStateToProps (state) {
  const {
    metamask: {
      selectedAddress,
      seedWords,
    },
    appState: {
      isLoading,
    },
  } = state

  return {
    seedWords,
    isLoading,
    address: selectedAddress,
  }
}

module.exports = compose(
  withRouter,
  connect(mapStateToProps)
)(BackupPhraseScreen)
