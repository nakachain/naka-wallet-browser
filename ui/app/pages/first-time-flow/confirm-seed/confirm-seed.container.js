import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose } from 'recompose'
import ConfirmSeedScreen from './confirm-seed.component'
import { confirmSeedWords } from '../../../actions'

function mapStateToProps (state) {
  const {
    metamask: {
      selectedAddress,
      seedWords,
    },
    appState: {
      isLoading,
    },
  } = state

  return {
    seedWords,
    isLoading,
    address: selectedAddress,
  }
}

function mapDispatchToProps (dispatch) {
  return {
    confirmSeedWords: () => dispatch(confirmSeedWords()),
  }
}

module.exports = compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(ConfirmSeedScreen)
