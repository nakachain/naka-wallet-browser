import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Markdown from 'react-markdown'
import {
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  FormGroup,
  FormControlLabel,
  Checkbox,
} from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import Identicon from '../../../components/identicon'
import LoadingScreen from '../../../components/loading-screen'
import { INITIALIZE_BACKUP_PHRASE_ROUTE } from '../../../routes'

export default class NoticeScreen extends Component {
  static contextTypes = {
    t: PropTypes.func,
  }

  static propTypes = {
    address: PropTypes.string.isRequired,
    location: PropTypes.shape({
      state: PropTypes.shape({
        next: PropTypes.func.isRequired,
      }),
    }),
    getAllNotices: PropTypes.func,
    notices: PropTypes.array,
    markAllNoticesRead: PropTypes.func,
    noActiveNotices: PropTypes.bool,
    history: PropTypes.object,
    isLoading: PropTypes.bool,
  };

  static defaultProps = {
    nextUnreadNotice: {},
  };

  state = {
    disclaimerChecked: false,
  }

  componentDidMount () {
    if (this.props.noActiveNotices) {
      this.props.history.push(INITIALIZE_BACKUP_PHRASE_ROUTE)
    }

    this.props.getAllNotices()
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  acceptTerms = async () => {
    const { markAllNoticesRead, history } = this.props

    markAllNoticesRead()
    .then(hasActiveNotices => {
      if (!hasActiveNotices) {
        history.push(INITIALIZE_BACKUP_PHRASE_ROUTE)
      }
    })
  }

  render () {
    const { address, notices, isLoading } = this.props
    const { disclaimerChecked } = this.state

    return (
      isLoading
        ? <LoadingScreen />
        : (
          <div>
            <div className="first-view-main-wrapper">
              <div className="first-view-main first-view-main__center">
                <div className="first-time-flow__main-container">
                  <div className="tou">
                    <Identicon
                      className="tou__icon"
                      address={address}
                      diameter={60}
                    />
                    {notices && notices.map((notice) => (
                        <ExpansionPanel key={notice.id}>
                          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                            {notice.title}
                          </ExpansionPanelSummary>
                          <ExpansionPanelDetails>
                            <Markdown
                              className="tou__body markdown"
                              source={notice.body}
                              skipHtml
                            />
                          </ExpansionPanelDetails>
                        </ExpansionPanel>
                      )
                    )}
                    <FormGroup row className="tou__disclaimer">
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={disclaimerChecked}
                            onChange={this.handleChange('disclaimerChecked')}
                            value="disclaimerChecked"
                          />
                        }
                        label={this.context.t('termsAccepted')}
                        color="primary"
                      />
                    </FormGroup>
                    <button
                      className="first-time-flow__button"
                      onClick={this.acceptTerms}
                      disabled={!disclaimerChecked}
                    >
                      Accept
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )
    )
  }
}
