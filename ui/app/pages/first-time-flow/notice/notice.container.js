import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose } from 'recompose'
import NoticeScreen from './notice.component'
import { getAllNotices, markAllNoticesRead } from '../../../actions'

function mapStateToProps (state) {
  const {
    metamask: {
      selectedAddress,
      notices,
      noActiveNotices,
    },
    appState: {
      isLoading,
    },
  } = state

  return {
    address: selectedAddress,
    notices,
    noActiveNotices,
    isLoading,
  }
}

function mapDispatchToProps (dispatch) {
  return {
    getAllNotices: () => dispatch(getAllNotices()),
    markAllNoticesRead: () => dispatch(markAllNoticesRead()),
  }
}

module.exports = compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(NoticeScreen)
