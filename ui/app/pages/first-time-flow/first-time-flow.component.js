import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Switch, Route } from 'react-router-dom'
import WelcomeScreen from './welcome'
import CreatePasswordScreen from './create-password'
import UniqueImageScreen from './unique-image'
import NoticeScreen from './notice'
import BackupPhraseScreen from './seed-phrase'
import ImportAccountScreen from './import-account/import-account.component'
import ImportSeedPhraseScreen from './import-seed-phrase'
import ConfirmSeed from './confirm-seed'
import {
  INITIALIZE_ROUTE,
  INITIALIZE_IMPORT_ACCOUNT_ROUTE,
  INITIALIZE_UNIQUE_IMAGE_ROUTE,
  INITIALIZE_IMPORT_WITH_SEED_PHRASE_ROUTE,
  INITIALIZE_NOTICE_ROUTE,
  INITIALIZE_BACKUP_PHRASE_ROUTE,
  INITIALIZE_CONFIRM_SEED_ROUTE,
  INITIALIZE_CREATE_PASSWORD_ROUTE,
} from '../../routes'

export default class FirstTimeFlow extends Component {

  static propTypes = {
    isInitialized: PropTypes.bool,
    seedWords: PropTypes.string,
    address: PropTypes.string,
    noActiveNotices: PropTypes.bool,
    goToBuyEtherView: PropTypes.func,
    isUnlocked: PropTypes.bool,
    history: PropTypes.object,
    welcomeScreenSeen: PropTypes.bool,
    isPopup: PropTypes.bool,
  };

  static defaultProps = {
    isInitialized: false,
    seedWords: '',
    noActiveNotices: false,
  };

  render () {
    return (
      <div className="first-time-flow-container flex-column flex-grow">
        <Switch>
          <Route exact path={INITIALIZE_IMPORT_ACCOUNT_ROUTE} component={ImportAccountScreen} />
          <Route
            exact
            path={INITIALIZE_IMPORT_WITH_SEED_PHRASE_ROUTE}
            component={ImportSeedPhraseScreen}
          />
          <Route exact path={INITIALIZE_UNIQUE_IMAGE_ROUTE} component={UniqueImageScreen} />
          <Route exact path={INITIALIZE_NOTICE_ROUTE} component={NoticeScreen} />
          <Route exact path={INITIALIZE_BACKUP_PHRASE_ROUTE} component={BackupPhraseScreen} />
          <Route exact path={INITIALIZE_CONFIRM_SEED_ROUTE} component={ConfirmSeed} />
          <Route exact path={INITIALIZE_CREATE_PASSWORD_ROUTE} component={CreatePasswordScreen} />
          <Route exact path={INITIALIZE_ROUTE} component={WelcomeScreen} />
        </Switch>
      </div>
    )
  }
}
