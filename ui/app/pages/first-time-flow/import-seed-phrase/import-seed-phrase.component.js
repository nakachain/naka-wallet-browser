import {validateMnemonic} from 'bip39'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { INITIALIZE_NOTICE_ROUTE } from '../../../routes'
import TextField from '../../../components/text-field'
import BackButton from '../../../components/back-button'

export default class ImportSeedPhraseScreen extends Component {
  static contextTypes = {
    t: PropTypes.func,
  }

  static propTypes = {
    warning: PropTypes.string,
    createNewVaultAndRestore: PropTypes.func.isRequired,
    leaveImportSeedScreenState: PropTypes.func,
    history: PropTypes.object,
    isLoading: PropTypes.bool,
  };

  state = {
    seedPhrase: '',
    password: '',
    confirmPassword: '',
    seedPhraseError: null,
    passwordError: null,
    confirmPasswordError: null,
  }

  handleBackButtonClick = (event) => {
    event.preventDefault()
    this.props.history.goBack()
  }

  parseSeedPhrase = (seedPhrase) => {
    return seedPhrase
      .trim()
      .match(/\w+/g)
      .join(' ')
  }

  handleSeedPhraseChange (seedPhrase) {
    let seedPhraseError = null

    if (seedPhrase) {
      const parsedSeedPhrase = this.parseSeedPhrase(seedPhrase)
      if (parsedSeedPhrase.split(' ').length !== 12) {
        seedPhraseError = this.context.t('seedPhraseReq')
      } else if (!validateMnemonic(parsedSeedPhrase)) {
        seedPhraseError = this.context.t('invalidSeedPhrase')
      }
    }

    this.setState({ seedPhrase, seedPhraseError })
  }

  handlePasswordChange (password) {
    const { confirmPassword } = this.state
    let confirmPasswordError = null
    let passwordError = null

    if (password && password.length < 8) {
      passwordError = this.context.t('passwordNotLongEnough')
    }

    if (confirmPassword && password !== confirmPassword) {
      confirmPasswordError = this.context.t('passwordsDontMatch')
    }

    this.setState({ password, passwordError, confirmPasswordError })
  }

  handleConfirmPasswordChange (confirmPassword) {
    const { password } = this.state
    let confirmPasswordError = null

    if (password !== confirmPassword) {
      confirmPasswordError = this.context.t('passwordsDontMatch')
    }

    this.setState({ confirmPassword, confirmPasswordError })
  }

  onClick = () => {
    const { password, seedPhrase } = this.state
    const {
      createNewVaultAndRestore,
      leaveImportSeedScreenState,
      history,
    } = this.props

    leaveImportSeedScreenState()
    createNewVaultAndRestore(password, this.parseSeedPhrase(seedPhrase))
      .then(() => history.push(INITIALIZE_NOTICE_ROUTE))
  }

  hasError () {
    const { passwordError, confirmPasswordError, seedPhraseError } = this.state
    return passwordError || confirmPasswordError || seedPhraseError
  }

  render () {
    const {
      seedPhrase,
      password,
      confirmPassword,
      seedPhraseError,
      passwordError,
      confirmPasswordError,
    } = this.state
    const { t } = this.context
    const { isLoading } = this.props
    const disabled = !seedPhrase || !password || !confirmPassword || isLoading || this.hasError()

    return (
      <div className="first-view-main-wrapper">
        <div className="first-view-main first-view-main__center">
          <div className="import-account first-time-flow__main-container">
            <BackButton onClick={this.handleBackButtonClick} />
            <div className="import-account__title">
              Import an Account with Seed Phrase
            </div>
            <div className="import-account__selector-label">
              Enter your secret twelve word phrase here to restore your vault.
            </div>
            <div className="import-account__input-wrapper">
              <label className="import-account__input-label">Wallet Seed</label>
              <textarea
                className="import-account__secret-phrase"
                onChange={e => this.handleSeedPhraseChange(e.target.value)}
                value={this.state.seedPhrase}
                placeholder="Separate each word with a single space"
              />
            </div>
            <span className="error">
              {seedPhraseError}
            </span>
            <div className="import-account__text-field-container">
              <TextField
                id="password"
                className="first-time-flow__input first-time-flow__margin-right"
                label={t('newPassword')}
                type="password"
                value={this.state.password}
                onChange={event => this.handlePasswordChange(event.target.value)}
                error={passwordError}
                autoComplete="new-password"
                margin="normal"
                largeLabel
              />
              <TextField
                id="confirm-password"
                className="first-time-flow__input"
                label={t('confirmPassword')}
                type="password"
                value={this.state.confirmPassword}
                onChange={event => this.handleConfirmPasswordChange(event.target.value)}
                error={confirmPasswordError}
                autoComplete="confirm-password"
                margin="normal"
                largeLabel
              />
            </div>
            <button
              className="first-time-flow__button"
              onClick={() => !disabled && this.onClick()}
              disabled={disabled}
            >
              Import
            </button>
          </div>
        </div>
      </div>
    )
  }
}
