import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose } from 'recompose'
import ImportSeedPhraseScreen from './import-seed-phrase.component'
import {
  createNewVaultAndRestore,
  unMarkPasswordForgotten,
} from '../../../actions'

function mapStateToProps (state) {
  const {
    appState: {
      warning,
      isLoading,
    },
  } = state

  return {
    warning,
    isLoading,
  }
}

function mapDispatchToProps (dispatch) {
  return {
    leaveImportSeedScreenState: () =>
      dispatch(unMarkPasswordForgotten()),
    createNewVaultAndRestore: (pw, seed) =>
      dispatch(createNewVaultAndRestore(pw, seed)),
  }
}

module.exports = compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(ImportSeedPhraseScreen)
