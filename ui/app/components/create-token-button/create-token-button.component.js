import PropTypes from 'prop-types'
import React, {PureComponent} from 'react'

export default class CreateTokenButton extends PureComponent {
  static contextTypes = {
    t: PropTypes.func.isRequired,
  }

  static defaultProps = {
    onClick: () => {},
  }

  static propTypes = {
    onClick: PropTypes.func,
  }

  render () {
    const { t } = this.context
    const { onClick } = this.props

    return (
      <div className="create-token-button">
        <p className="create-token-button__help-desc">{t('clickToCreate', [t('createToken')])}</p>
        <div
          className="create-token-button__button"
          onClick={onClick}
        >
          {t('createToken')}
        </div>
      </div>
    )
  }
}
