import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

import CurrencyDisplay from '../currency-display'
import { PRIMARY, SECONDARY, NAKA } from '../../constants/common'
import { getConfirmPageTotalText } from '../../helpers/confirm-transaction/util'
import { isPayByToken } from '../../helpers/transactions.util'

export default class UserPreferencedCurrencyDisplay extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    prefix: PropTypes.string,
    value: PropTypes.string,
    numberOfDecimals: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    hideLabel: PropTypes.bool,
    style: PropTypes.object,
    showEthLogo: PropTypes.bool,
    ethLogoHeight: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    // Used in container
    type: PropTypes.oneOf([PRIMARY, SECONDARY]),
    ethNumberOfDecimals: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    fiatNumberOfDecimals: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    ethPrefix: PropTypes.string,
    fiatPrefix: PropTypes.string,
    // From container
    currency: PropTypes.string,
    nativeCurrency: PropTypes.string,
  }

  renderEthLogo () {
    const { currency, showEthLogo, ethLogoHeight = 12 } = this.props
    return currency === NAKA && showEthLogo && (
      <img
        src="/images/logo/naka_symbol.svg"
        height={ethLogoHeight}
      />
    )
  }

  renderTokenSymbol () {
    const {
      showTokenSymbol,
      payByTokenProps,
      ethLogoHeight = 12,
    } = this.props
    const symbol = isPayByToken(payByTokenProps)
      ? payByTokenProps.tokenSymbol
      : NAKA

    return showTokenSymbol && (
      <span height={ethLogoHeight}>
        {symbol}
      </span>
    )
  }

  renderSendNakaConfirmPage () {
    const {
      showSendNakaConfirmPage,
      ethLogoHeight = 12,
    } = this.props
    const tokensText = getConfirmPageTotalText(this.props)

    return showSendNakaConfirmPage && (
      <span height={ethLogoHeight}>
        {tokensText}
      </span>
    )
  }

  render () {
    return (
      <CurrencyDisplay
        {...this.props}
        prefixComponent={this.renderEthLogo()}
        suffixComponent={this.renderTokenSymbol()}
        confirmComponent={this.renderSendNakaConfirmPage()}
      />
    )
  }
}
