import assert from 'assert'
import proxyquire from 'proxyquire'

let mapStateToProps, mergeProps

proxyquire('../user-preferenced-currency-display.container.js', {
  'react-redux': {
    connect: (ms, md, mp) => {
      mapStateToProps = ms
      mergeProps = mp
      return () => ({})
    },
  },
})

describe('UserPreferencedCurrencyDisplay container', () => {
  describe('mapStateToProps()', () => {
    it('should return the correct props', () => {
      const mockState = {
        metamask: {
          nativeCurrency: 'NAKA',
          preferences: {
            useNativeCurrencyAsPrimaryCurrency: true,
          },
        },
      }

      assert.deepEqual(mapStateToProps(mockState), {
        nativeCurrency: 'NAKA',
        useNativeCurrencyAsPrimaryCurrency: true,
      })
    })
  })

  describe('mergeProps()', () => {
    it('should return the correct props', () => {
      const mockDispatchProps = {}

      const tests = [
        {
          stateProps: {
            useNativeCurrencyAsPrimaryCurrency: true,
            nativeCurrency: 'NAKA',
          },
          ownProps: {
            type: 'PRIMARY',
          },
          result: {
            currency: 'NAKA',
            nativeCurrency: 'NAKA',
            numberOfDecimals: 6,
            prefix: undefined,
          },
        },
        {
          stateProps: {
            useNativeCurrencyAsPrimaryCurrency: false,
            nativeCurrency: 'NAKA',
          },
          ownProps: {
            type: 'PRIMARY',
          },
          result: {
            currency: undefined,
            nativeCurrency: 'NAKA',
            numberOfDecimals: 2,
            prefix: undefined,
          },
        },
        {
          stateProps: {
            useNativeCurrencyAsPrimaryCurrency: true,
            nativeCurrency: 'NAKA',
          },
          ownProps: {
            type: 'SECONDARY',
            fiatNumberOfDecimals: 4,
            fiatPrefix: '-',
          },
          result: {
            nativeCurrency: 'NAKA',
            currency: undefined,
            numberOfDecimals: 4,
            prefix: '-',
          },
        },
        {
          stateProps: {
            useNativeCurrencyAsPrimaryCurrency: false,
            nativeCurrency: 'NAKA',
          },
          ownProps: {
            type: 'SECONDARY',
            fiatNumberOfDecimals: 4,
            numberOfDecimals: 3,
            fiatPrefix: 'a',
            prefix: 'b',
          },
          result: {
            currency: 'NAKA',
            nativeCurrency: 'NAKA',
            numberOfDecimals: 3,
            prefix: 'b',
          },
        },
      ]

      tests.forEach(({ stateProps, ownProps, result }) => {
        assert.deepEqual(mergeProps({ ...stateProps }, mockDispatchProps, { ...ownProps }), {
          ...result,
        })
      })
    })
  })
})
