import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import { getTransactionActionKey } from '../../helpers/transactions.util'
import { camelCaseToCapitalize } from '../../helpers/common.util'
import { TRANSACTION_FEE_EXCHANGE, EXCHANGE_INFO } from '../../constants/transactions'
export default class TransactionAction extends PureComponent {
  static contextTypes = {
    t: PropTypes.func,
  }

  static propTypes = {
    className: PropTypes.string,
    transaction: PropTypes.object,
    methodData: PropTypes.object,
    displayValues: PropTypes.object,
    token: PropTypes.object,
  }

  state = {
    transactionAction: '',
    extraInfo: undefined,
  }

  componentDidMount () {
    this.getTransactionAction()
  }

  componentDidUpdate () {
    this.getTransactionAction()
  }

  async getTransactionAction () {
    const { transaction, methodData, displayValues, token } = this.props
    const { tokenTransfers } = transaction
    const { data, done } = methodData
    const { name = '' } = data
    const symbol = token ? token.symbol : tokenTransfers && tokenTransfers.length > 0 ? tokenTransfers[0].token.symbol : ''
    if (!done) {
      return
    }
    let extraInfo
    const actionKey = await getTransactionActionKey(transaction, data, displayValues)

    if (actionKey === TRANSACTION_FEE_EXCHANGE) extraInfo = this.context.t(EXCHANGE_INFO, [symbol])
    const action = actionKey
      ? this.context.t(actionKey)
      : camelCaseToCapitalize(name)

    this.setState({ transactionAction: action, extraInfo: extraInfo })
  }

  render () {
    const { className, methodData: { done } } = this.props
    const { transactionAction, extraInfo } = this.state

    return (
      <div className={className}>
      <div className={classnames('transaction-action', className)}>
        { (done && transactionAction) || '--' }
      </div>
      <div className={classnames('transaction-list-item__nonce', className)}>
        { extraInfo }
      </div>
      </div>
    )
  }
}
