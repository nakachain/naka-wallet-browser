import { connect } from 'react-redux'
import ConfirmTokenTransactionBase from './confirm-token-transaction-base.component'
import {
  tokenAmountAndToAddressSelector,
  contractExchangeRateSelector,
} from '../../../selectors/confirm-transaction'

const mapStateToProps = (state, ownProps) => {
  const { tokenAmount: ownTokenAmount } = ownProps
  const {
    confirmTransaction,
    metamask: {
      currentCurrency,
      conversionRate,
      selectedTokenAddress,
    },
  } = state
  const {
    txData: { txParams: { to: tokenAddress } = {} } = {},
    tokenData,
    tokenProps,
    fiatTransactionTotal,
    ethTransactionAmount,
    ethTransactionFee,
    ethTransactionTotal,
    hexTransactionAmount,
    hexTransactionFee,
    hexTransactionTotal,
    payByTokenProps,
  } = confirmTransaction

  const { tokenAmount, toAddress } = tokenAmountAndToAddressSelector(state)
  const contractExchangeRate = contractExchangeRateSelector(state)

  return {
    toAddress,
    tokenAddress,
    tokenAmount: typeof ownTokenAmount !== 'undefined' ? ownTokenAmount : tokenAmount,
    tokenData,
    tokenProps,
    currentCurrency,
    conversionRate,
    contractExchangeRate,
    fiatTransactionTotal,
    ethTransactionAmount,
    ethTransactionFee,
    ethTransactionTotal,
    hexTransactionAmount,
    hexTransactionFee,
    hexTransactionTotal,
    selectedTokenAddress,
    payByTokenProps,
  }
}

export default connect(mapStateToProps)(ConfirmTokenTransactionBase)
