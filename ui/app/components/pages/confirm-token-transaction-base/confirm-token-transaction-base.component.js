import React, { Component } from 'react'
import PropTypes from 'prop-types'

import ConfirmTransactionBase from '../confirm-transaction-base'
import UserPreferencedCurrencyDisplay from '../../user-preferenced-currency-display'
import {
  formatCurrency,
  convertTokenToFiat,
  addFiat,
  roundExponential,
  getConfirmPageTotalText,
} from '../../../helpers/confirm-transaction/util'
import { getWeiHexFromDecimalValue } from '../../../helpers/conversions.util'
import { NAKA, PRIMARY } from '../../../constants/common'

export default class ConfirmTokenTransactionBase extends Component {
  static contextTypes = {
    t: PropTypes.func,
  }

  static propTypes = {
    toAddress: PropTypes.string,
    tokenAddress: PropTypes.string,
    tokenAmount: PropTypes.number,
    tokenData: PropTypes.object,
    tokenProps: PropTypes.object,
    fiatTransactionTotal: PropTypes.string,
    ethTransactionTotal: PropTypes.string,
    contractExchangeRate: PropTypes.number,
    conversionRate: PropTypes.number,
    currentCurrency: PropTypes.string,
  }

  getFiatTransactionAmount () {
    const { tokenAmount, currentCurrency, conversionRate, contractExchangeRate } = this.props

    return convertTokenToFiat({
      value: tokenAmount,
      toCurrency: currentCurrency,
      conversionRate,
      contractExchangeRate,
    })
  }

  renderSubtitleComponent () {
    const { contractExchangeRate, tokenAmount } = this.props

    const decimalEthValue = (tokenAmount * contractExchangeRate) || 0
    const hexWeiValue = getWeiHexFromDecimalValue({
      value: decimalEthValue,
      fromCurrency: NAKA,
      fromDenomination: NAKA,
    })

    return typeof contractExchangeRate === 'undefined'
      ? (
        <span>
          { this.context.t('noConversionRateAvailable') }
        </span>
      ) : (
        <UserPreferencedCurrencyDisplay
          value={hexWeiValue}
          type={PRIMARY}
          showEthLogo
          hideLabel
        />
      )
  }

  renderPrimaryTotalTextOverride () {
    const tokensText = getConfirmPageTotalText(this.props)

    return (
      <div>
        <span>{ `${tokensText}` }</span>
      </div>
    )
  }

  getSecondaryTotalTextOverride () {
    const { fiatTransactionTotal, currentCurrency, contractExchangeRate } = this.props

    if (typeof contractExchangeRate === 'undefined') {
      return formatCurrency(fiatTransactionTotal, currentCurrency)
    } else {
      const fiatTransactionAmount = this.getFiatTransactionAmount()
      const fiatTotal = addFiat(fiatTransactionAmount, fiatTransactionTotal)
      const roundedFiatTotal = roundExponential(fiatTotal)
      return formatCurrency(roundedFiatTotal, currentCurrency)
    }
  }

  render () {
    const {
      toAddress,
      tokenAddress,
      tokenAmount,
      tokenProps,
      ...restProps
    } = this.props

    const tokensText = `${tokenAmount} ${tokenProps.tokenSymbol}`

    return (
      <ConfirmTransactionBase
        toAddress={toAddress}
        identiconAddress={tokenAddress}
        title={tokensText}
        subtitleComponent={this.renderSubtitleComponent()}
        primaryTotalTextOverride={this.renderPrimaryTotalTextOverride()}
        secondaryTotalTextOverride={this.getSecondaryTotalTextOverride()}
        {...restProps}
      />
    )
  }
}
