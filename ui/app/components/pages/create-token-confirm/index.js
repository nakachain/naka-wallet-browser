import React, { Component } from 'react'
const connect = require('react-redux').connect
const actions = require('../../../actions')
const Web3EthAbi = require('web3-eth-abi')
import NRC223 from '../../../../../config/contracts/nrc223'
import { Typography, Button, withStyles } from '@material-ui/core'
import styles from './styles'
import { DEFAULT_ROUTE, CONFIRM_TRANSACTION_ROUTE } from '../../../routes'

// NAKA: change name, this is not used for create token. used for manage token.
class CreateTokenConfirm extends Component {
  state = {
    open: false,
  };
  handleClickOpen = async () => {
    const { name, totalSupply, precision, symbol } = this.props.specs
    const { history } = this.props
    var params = Web3EthAbi.encodeParameters(
      ['string', 'string', 'uint8', 'uint256', 'address'],
      [name, symbol, precision, totalSupply, this.props.selectedAddress]
      )
      params = params.substring(2)
      const ss = {
        from: this.props.selectedAddress,
        gas: '211234',
        data: NRC223.bytecode + params,
      }
      const promise = await this.props.sign(ss)
      Promise.resolve(promise)
      .then(() => history.push(CONFIRM_TRANSACTION_ROUTE))
    this.setState({ open: true })
  };
   handleClose = () => {

    this.setState({ open: false })
  };

  render () {
    const { classes, specs } = this.props

    return (
      <div className={classes.createTokenContainer}>
        <Typography style={{width: '100%', textAlign: 'left', padding: '10px'}}>
          <Button mini onClick={() => this.props.history.goBack()}><img src="/images/back.svg" style={{ verticalAlign: 'middle'}} /></Button> Create New Token
        </Typography>
        <Field start="Name" value={specs.name} />
        <Field start="Total Supply" value={Number(specs.totalSupply).toLocaleString()} />
        <Field start="Precision" value={`${specs.precision} DIGITAL`} />
        <Field start="Symbol" value={specs.symbol} />
        <Field start="Exchange Rate" value={specs.rate} />
        <Field start="Tx Fee" value={specs.txFee} />
        <Typography style={{width: '100%', textAlign: 'center', padding: '10px'}}>
          <Button variant="extendedFab" fullWidth className={classes.gradientButton} onClick={this.handleClickOpen}>Confirm</Button>
        </Typography>
      </div>
     )
  }
}
  const Field = withStyles(styles, { withTheme: true })(({start, value, classes}) => {
  return (
    <div className={classes.field}>
      <span style={{float: 'left'}}>{start}</span>
      <span style={{float: 'right'}}>{value}</span>
      </div>
  )
}
   )
   const mapStateToProps = state => {
    const { appState } = state
     return {
      specs: appState.specs,
      selectedAddress: state.metamask.selectedAddress,
    }
  }
   const mapDispatchToProps = dispatch => ({
    sign: (ss) => {
        dispatch(actions.signTx(ss))
},
  })
  function constructTxParams ({ selectedToken, data, to, amount, from, gas, gasPrice }) {
    const txParams = {
      data,
      from,
      value: '0',
      gas,
      gasPrice,
    }

    if (!selectedToken) {
      txParams.value = amount
      txParams.to = to
    }

    return addHexPrefixToObjectValues(txParams)
  }
 module.exports = connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(CreateTokenConfirm))
