
const styles = (theme) => ({
  field: {
    height: '24px',
    borderBottom: '2px solid rgba(0, 0, 0, 0.1)',
    fontSize: '12px',
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    margin: '30px 10px',
  },
  createTokenContainer: {
    textAlign: 'center',
    width: '90%',
  },
  gradientButton: {
    color: 'white',
    backgroundImage: 'linear-gradient(#5539DF, #8E6BF1)',
  },
})
 export default styles
