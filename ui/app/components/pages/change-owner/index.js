import React, { Component } from 'react'
const connect = require('react-redux').connect
import { isEmpty } from 'lodash'
import PropTypes from 'prop-types'
const actions = require('../../../actions')
import { Typography, TextField, Button, withStyles } from '@material-ui/core'
import styles from './styles'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import TransactionFee from '../transaction-fee'
import { CREATE_NEW_TOKEN_CONFIRM } from '../../../routes'
import NumberFormat from 'react-number-format'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

class ChangeOwner extends Component {
  state = {
    open: false,
    name: '',
    totalSupply: '',
    precision: '',
    symbol: '',
    txFee: '0',
  };
   handleClickOpen = () => {
    this.setState({ open: true })
  };
   handleClose = () => {
    this.setState({ open: false })
  };
   handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    })
  };
   handleConfirm = () => {
    const { name, totalSupply, precision, symbol, txFee } = this.state
    this.props.createNewToken(name, totalSupply, precision, symbol, txFee)
    this.props.history.push(CREATE_NEW_TOKEN_CONFIRM)
  }
   render () {
    const { classes } = this.props
    const { name, totalSupply, precision, symbol } = this.state
    const isInvalid = isEmpty(name) || isEmpty(totalSupply) || isEmpty(precision) || isEmpty(symbol)

    return (
      <div className={classes.createTokenContainer}>
        <Typography style={{width: '100%', textAlign: 'left', padding: '10px'}}>
          <Button mini onClick={() => this.props.history.goBack()}><img src="/images/back.svg" style={{ verticalAlign: 'middle'}} /></Button> Create New Token
        </Typography>
        <NewTokenField label="Owner" defaultValue="" handleChange={this.handleChange}/>
        <TextField
          label="Transaction Fee"
          fullWidth
          InputProps={{
            disableUnderline: true,
            classes: {
              root: classes.bootstrapRoot,
              input: classes.bootstrapInput,
            },
            inputComponent: TransactionFeeButton,
            value: '5 NAKA',
            endAdornment: <ExpandMoreIcon style={{position: 'absolute', right: '30px' }}/>,
            onClick: this.handleClickOpen,
          }}
          classes= {{root: classes.textField}}
          InputLabelProps={{
            shrink: true,
            className: classes.bootstrapFormLabel,
          }}
        />
        <Typography style={{width: '100%', textAlign: 'center', padding: '10px'}}>
          <Button disabled = {isInvalid} variant="extendedFab" fullWidth className={classes.gradientButton} onClick={this.handleConfirm}> CREATE </Button>
        </Typography>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          maxWidth="md"
          fullWidth
        >
          <DialogTitle classes={{root: classes.dialogTitleRoot}} >{'Transaction Fee'}</DialogTitle>
          <DialogContent classes={{root: classes.dialogContentRoot}} >
            <TransactionFee handleChange={this.handleChange} value={this.state.txFee}/>
          </DialogContent>
          <DialogActions>
            <Button variant="extendedFab" classes={{extendedFab: classes.smallerExtendButton }} size="small" fullWidth className={classes.gradientButton} onClick={this.handleClose} autoFocus>
              OK
            </Button>
          </DialogActions>
        </Dialog>
      </div>
     )
  }
   static contextTypes = {
    t: PropTypes.func,
  }
}
const TransactionFeeButton = withStyles(styles)(({classes, value}) => {
  return (
      <Button fullWidth classes={{label: classes.buttonLabel}} variant="outlined"> {value}</Button>
          )
        })
const NewTokenField = withStyles(styles)(({classes, defaultValue, type, label, startAdornment, endAdornment, handleChange, ...props}) => {
const start = startAdornment &&
  (
    <Typography className={classes.startFieldTextAdornment}>
      1 {startAdornment} =
    </Typography>
  )
const end = endAdornment &&
  (
    <Typography className={classes.fieldTextAdornment}>
      {endAdornment}
    </Typography>
  )
  let supply = ''
  let inputComponent = 'input'
  if (label === 'totalSupply') {
    supply = 'total Supply'
    inputComponent = NumberFormatCustom
  }
  return (
    <TextField
      placeholder={defaultValue}
      label={label === 'totalSupply' ? supply : label}
      id="bootstrap-input"
      type={type}
      required
      classes= {{root: classes.textField}}
      InputProps={{
        disableUnderline: true,
        classes: {
          root: classes.bootstrapRoot,
          input: classes.bootstrapInput,
        },
        inputComponent,
        startAdornment: start,
        endAdornment: end,
      }}
      InputLabelProps={{
        shrink: true,
        className: classes.bootstrapFormLabel,
      }}
      onChange={handleChange(label)}
     />
  )
  })


const mapStateToProps = state => ({
  selectedAddress: state.metamask.selectedAddress,
})
const mapDispatchToProps = dispatch => ({
  createNewToken: (name, totalSupply, precision, symbol, txFee) => dispatch(actions.createNewToken(name, totalSupply, precision, symbol, txFee)),
})
module.exports = connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ChangeOwner))
