const { Component } = require('react')
const PropTypes = require('prop-types')
const { INITIALIZE_NOTICE_ROUTE } = require('../../routes')

class Notice extends Component {
  componentWillMount () {
    this.props.history.push(INITIALIZE_NOTICE_ROUTE)
  }

  render () {
    return null
  }
}

Notice.propTypes = {
  history: PropTypes.object,
}

module.exports = Notice
