import React, { Component } from 'react'
const connect = require('react-redux').connect
import { isEmpty } from 'lodash'
import PropTypes from 'prop-types'
const actions = require('../../../actions')
import { Typography, TextField, Button, withStyles } from '@material-ui/core'
import styles from './styles'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Divider from '@material-ui/core/Divider'
import { CHANGE_OWNER, CHANGE_RATE } from '../../../routes'

class TokenManage extends Component {
  state = {
    open: false,
    name: '',
    totalSupply: '',
    precision: '',
    symbol: '',
    txFee: '0',
  };
   handleClickOpen = () => {
    this.setState({ open: true })
  };
   handleClose = () => {
    this.setState({ open: false })
  };
   handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    })
  };
   handleConfirm = () => {
    const { name, totalSupply, precision, symbol, txFee } = this.state
    this.props.createNewToken(name, totalSupply, precision, symbol, txFee)
    this.props.history.push(CREATE_NEW_TOKEN_CONFIRM)
  }

   render () {
    const { classes } = this.props
    const { name, totalSupply, precision, symbol } = this.state
    const isInvalid = isEmpty(name) || isEmpty(totalSupply) || isEmpty(precision) || isEmpty(symbol)

    return (
      <div className={classes.createTokenContainer}>
        <Typography style={{width: '100%', textAlign: 'left', padding: '10px'}}>
          <Button mini onClick={() => this.props.history.goBack()}><img src="/images/back.svg" style={{ verticalAlign: 'middle'}} /></Button> Manage
        </Typography>
      </div>
     )
  }
   static contextTypes = {
    t: PropTypes.func,
  }
}


const mapDispatchToProps = dispatch => ({
  getRate: (tokenAddress, exchangerAddress) => dispatch(actions.getRate(tokenAddress, exchangerAddress)),
})

function mapStateToProps (state) {
  return {
    payByTokens: state.metamask.payByTokens,
    gasTotal: state.metamask.send.gasTotal,
    selectedToken: state.metamask.selectedToken,
  }
}
module.exports = connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(TokenManage))
