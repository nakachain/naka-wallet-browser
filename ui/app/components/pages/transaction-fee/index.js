import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormHelperText from '@material-ui/core/FormHelperText'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormControl from '@material-ui/core/FormControl'
import FormLabel from '@material-ui/core/FormLabel'
 const styles = theme => ({
  root: {
    display: 'flex',
  },
  formControl: {
    margin: theme.spacing.unit * 3,
  },
  group: {
    margin: `${theme.spacing.unit}px 0`,
  },
  label: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
  },
})
 class RadioButtonsGroup extends React.Component {
  render () {
    const { classes, handleChange, value } = this.props
     return (
      <div className={classes.root}>
        <FormControl component="fieldset" className={classes.formControl}>
          <RadioGroup
            aria-label="Gender"
            name="txFee"
            className={classes.group}
            value={value}
            onChange={handleChange('txFee')}
          >
            <FormControlLabel value="0" classes={{label: classes.label}} control={<Radio />} label="0.5 BOT" />
            <FormControlLabel value="1" classes={{label: classes.label}} control={<Radio />} label="5 NAKA" />
          </RadioGroup>
        </FormControl>
      </div>
    )
  }
}
 RadioButtonsGroup.propTypes = {
  classes: PropTypes.object.isRequired,
}
 export default withStyles(styles)(RadioButtonsGroup)
