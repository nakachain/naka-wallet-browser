import React from 'react'
import PropTypes from 'prop-types'
import PersistentForm from '../../../../lib/persistent-form'
import {
  getAmountErrorObject,
  getGasFeeErrorObject,
  getToAddressForGasUpdate,
  doesAmountErrorRequireUpdate,
} from '../../compose/send.utils.js'

import SendHeader from '../../compose/send-header'
import SendContent from '../../compose/send-content'
import SendFooter from '../../compose/send-footer'
import { CREATE_TOKEN } from '../../compose/send.constants'

export default class CreateToken extends PersistentForm {

  static propTypes = {
    amount: PropTypes.string,
    amountConversionRate: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
    blockGasLimit: PropTypes.string,
    conversionRate: PropTypes.number,
    editingTransactionId: PropTypes.string,
    from: PropTypes.object,
    gasLimit: PropTypes.string,
    gasPrice: PropTypes.string,
    gasTotal: PropTypes.string,
    history: PropTypes.object,
    network: PropTypes.string,
    primaryCurrency: PropTypes.string,
    recentBlocks: PropTypes.array,
    selectedAddress: PropTypes.string,
    selectedToken: PropTypes.object,
    tokenBalance: PropTypes.string,
    tokenContract: PropTypes.object,
    fetchBasicGasEstimates: PropTypes.func,
    updateAndSetGasTotal: PropTypes.func,
    updateSendErrors: PropTypes.func,
    updateSendTokenBalance: PropTypes.func,
    scanQrCode: PropTypes.func,
    qrCodeDetected: PropTypes.func,
    qrCodeData: PropTypes.object,
  }

  static contextTypes = {
    t: PropTypes.func,
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.qrCodeData) {
      if (nextProps.qrCodeData.type === 'address') {
        const scannedAddress = nextProps.qrCodeData.values.address.toLowerCase()
        const currentAddress = this.props.to && this.props.to.toLowerCase()
        if (currentAddress !== scannedAddress) {
          this.props.updateSendTo(scannedAddress)
          this.updateGas({ to: scannedAddress })
          // Clean up QR code data after handling
          this.props.qrCodeDetected(null)
        }
      }
    }
  }

  updateGas ({ to: updatedToAddress, amount: value } = {}) {
    const {
      amount,
      blockGasLimit,
      editingTransactionId,
      gasLimit,
      gasPrice,
      data,
      recentBlocks,
      selectedAddress,
      selectedToken = {},
      to: currentToAddress,
      updateAndSetGasLimit,
    } = this.props
    updateAndSetGasLimit({
      blockGasLimit,
      editingTransactionId,
      gasLimit,
      gasPrice,
      recentBlocks,
      selectedAddress,
      selectedToken,
      to: getToAddressForGasUpdate(updatedToAddress, currentToAddress),
      value: value || amount,
      data,
    })
  }

  componentDidUpdate (prevProps) {
    const {
      amount,
      amountConversionRate,
      conversionRate,
      from: { address, balance },
      gasTotal,
      network,
      primaryCurrency,
      selectedToken,
      tokenBalance,
      updateSendErrors,
      updateSendTokenBalance,
      tokenContract,
      selectedPayToken,
      exchangeRate,
    } = this.props
    const {
      from: { balance: prevBalance },
      gasTotal: prevGasTotal,
      tokenBalance: prevTokenBalance,
      network: prevNetwork,
      selectedPayToken: prevSelectedPayToken,
    } = prevProps

    const uninitialized = [prevBalance, prevGasTotal].every(n => n === null)

    const amountErrorRequiresUpdate = doesAmountErrorRequireUpdate({
      balance,
      gasTotal,
      prevBalance,
      prevGasTotal,
      prevTokenBalance,
      selectedToken,
      tokenBalance,
      prevSelectedPayToken,
      selectedPayToken,
    })

    if (amountErrorRequiresUpdate) {
      const amountErrorObject = getAmountErrorObject({
        amount,
        amountConversionRate,
        balance,
        conversionRate,
        gasTotal,
        primaryCurrency,
        selectedToken,
        tokenBalance,
      })
      const gasFeeErrorObject =
        getGasFeeErrorObject({
          amountConversionRate,
          balance,
          conversionRate,
          gasTotal,
          primaryCurrency,
          selectedPayToken,
          exchangeRate,
        })
      updateSendErrors(Object.assign(amountErrorObject, gasFeeErrorObject))
    }

    if (!uninitialized) {

      if (network !== prevNetwork && network !== 'loading') {
        updateSendTokenBalance({
          selectedToken,
          tokenContract,
          address,
        })
        this.updateGas()
      }
    }
  }

  componentDidMount () {
    const {
      selectedAddress,
      updateCreateTokenOwner,
      fetchBasicGasEstimates,
    } = this.props

    updateCreateTokenOwner(selectedAddress)
    fetchBasicGasEstimates()
    .then(() => {
      this.updateGas()
    })
  }

  componentWillMount () {
    const {
      from: { address },
      selectedToken,
      tokenContract,
      updateSendTokenBalance,
    } = this.props

    updateSendTokenBalance({
      selectedToken,
      tokenContract,
      address,
    })
    this.updateGas()

    // Show QR Scanner modal  if ?scan=true
    if (window.location.search === '?scan=true') {
      this.props.scanQrCode()

      // Clear the queryString param after showing the modal
      const cleanUrl = location.href.split('?')[0]
      history.pushState({}, null, `${cleanUrl}`)
      window.location.hash = '#send'
    }
  }

  componentWillUnmount () {
    this.props.resetSendState()
  }

  render () {
    const { history, showHexData } = this.props

    return (
      <div className="page-container">
        <SendHeader history={history} type={CREATE_TOKEN} />
        <SendContent
          updateGas={(updateData) => this.updateGas(updateData)}
          scanQrCode={_ => this.props.scanQrCode()}
          showHexData={showHexData}
          type={CREATE_TOKEN}
        />
        <SendFooter history={history} type={CREATE_TOKEN} />
      </div>
    )
  }
}
