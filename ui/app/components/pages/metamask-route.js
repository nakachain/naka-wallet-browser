const PropTypes = require('prop-types')
const { Route } = require('react-router-dom')
const h = require('react-hyperscript')

const MetamaskRoute = ({ component, ...props }) => {
  return (
    h(Route, {
      ...props,
      component,
    })
  )
}

MetamaskRoute.propTypes = {
  component: PropTypes.func,
}

module.exports = MetamaskRoute
