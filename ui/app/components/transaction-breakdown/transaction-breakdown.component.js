import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import TransactionBreakdownRow from './transaction-breakdown-row'
import CurrencyDisplay from '../currency-display'
import TokenCurrencyDisplay from '../token-currency-display'
import HexToDecimal from '../hex-to-decimal'
import { GWEI } from '../../constants/common'
import { getHexGasTotal } from '../../helpers/confirm-transaction/util'
import { sumHexes } from '../../helpers/transactions.util'
import { calcTokenAmount } from '../../token-util'

export default class TransactionBreakdown extends PureComponent {
  static contextTypes = {
    t: PropTypes.func,
  }

  static propTypes = {
    transaction: PropTypes.object,
    className: PropTypes.string,
    nativeCurrency: PropTypes.string.isRequired,
    transactionInfo: PropTypes.object,
  }

  static defaultProps = {
    transaction: {},
  }

  render () {
    const { t } = this.context
    const { transaction, className, nativeCurrency, transactionInfo } = this.props
    const { nakaSent, nakaReceived, tokenSent, tokenReceived, gasFeeSent, gasFeeReceived } = transactionInfo
    const { txParams: { gas, gasPrice, value, exchangeRate, exchanger } = {}, txReceipt: { gasUsed } = {} } = transaction

    const gasLimit = typeof gasUsed === 'string' ? gasUsed : gas

    const hexGasTotal = getHexGasTotal({ gasLimit, gasPrice })
    const totalInHex = sumHexes(hexGasTotal, value)
    const decimals = Number(18)
    const exchangeRateDisplay = exchangeRate ? calcTokenAmount(exchangeRate, decimals) : 0

    const amount = nakaReceived || nakaSent || tokenReceived || tokenSent
    if ((nakaReceived && nakaSent) || (tokenReceived && tokenSent)) amount.display = '0' // in case A send token to A, with exchanger of A

    const gasFee = gasFeeSent || gasFeeReceived
    if (gasFeeSent && gasFeeReceived) gasFee.display = '0'

    let total
    if (amount && gasFee) {
      if (amount.symbol === gasFee.symbol) {
        total = {
          display: (parseFloat(amount.display) + parseFloat(gasFee.display)).toString(),
          symbol: amount.symbol,
        }
      } else {
        if (amount.display === '0' && gasFee.display === '0') {
          total = {
            display: '0',
            symbol: amount.symbol,
          }
        } else if (amount.display === '0') total = gasFee
        else if (gasFee.display === '0') total = amount
      }
    } else if (amount) total = amount
    else if (gasFee) total = gasFee


    return (
      <div className={classnames('transaction-breakdown', className)}>
        <div className="transaction-breakdown__title">
          { t('transaction') }
        </div>
        {
          amount && (
            <TransactionBreakdownRow title={t('amount')}>
              <TokenCurrencyDisplay
                className="transaction-breakdown__value"
                displayValues={{ amount }}
              />
            </TransactionBreakdownRow>
          )
        }
        {
          (gasFeeReceived || gasFeeSent) && (
            <TransactionBreakdownRow
              title={`${t('gasLimit')} (${t('units')})`}
              className="transaction-breakdown__row-title"
            >
              <HexToDecimal
                className="transaction-breakdown__value"
                value={gas}
              />
            </TransactionBreakdownRow>
          )
        }
        {
          (gasFeeReceived || gasFeeSent) && (
            <TransactionBreakdownRow
              title={`${t('gasUsed')} (${t('units')})`}
              className="transaction-breakdown__row-title"
            >
              <HexToDecimal
                className="transaction-breakdown__value"
                value={gasUsed}
              />
            </TransactionBreakdownRow>
          )
        }
        {
          (gasFeeReceived || gasFeeSent) && (
            <TransactionBreakdownRow title={t('gasPrice')}>
              <CurrencyDisplay
                className="transaction-breakdown__value"
                currency={nativeCurrency}
                denomination={GWEI}
                value={gasPrice}
                hideLabel
              />
            </TransactionBreakdownRow>
          )
        }
        {
          exchanger && (
            <TransactionBreakdownRow
              title={t('exchanger')}
              className="transaction-breakdown__row-title"
            >
              <div className="transaction-breakdown__address">{exchanger}</div>
            </TransactionBreakdownRow>
          )
        }
        {
          exchangeRate && exchangeRateDisplay !== 0 && (
            <TransactionBreakdownRow
              title={t('exchangeRate')}
              className="transaction-breakdown__row-title"
            >
              {exchangeRateDisplay.toString()}
            </TransactionBreakdownRow>
          )
        }
        <TransactionBreakdownRow title={t('total')}>
          {
            total ? (
              <TokenCurrencyDisplay
                className="transaction-breakdown__value"
                displayValues={{ total }}
              />
            ) : (
              <div className="transaction-breakdown__value">
                {amount && gasFee && `${amount.display} ${amount.symbol} ${gasFee.display} ${gasFee.symbol}`}
              </div>
            )
          }
        </TransactionBreakdownRow>
      </div>
    )
  }
}
