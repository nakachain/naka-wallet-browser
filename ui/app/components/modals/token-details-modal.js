const Component = require('react').Component
const PropTypes = require('prop-types')
const h = require('react-hyperscript')
const inherits = require('util').inherits
const connect = require('react-redux').connect
const actions = require('../../actions')
const AccountModalContainer = require('./account-modal-container')
const QrView = require('../qr-code')
import Identicon from '../identicon'
import Button from '../button'

function mapStateToProps (state) {
  return {
    token: state.appState.modal.modalState.props.token,
  }
}

function mapDispatchToProps (dispatch) {
  return {
    showQrView: (selected, identity) => dispatch(actions.showQrView(selected, identity)),
    hideModal: () => dispatch(actions.hideModal()),
  }
}

inherits(AccountDetailsModal, Component)
function AccountDetailsModal () {
  Component.call(this)
}

AccountDetailsModal.contextTypes = {
  t: PropTypes.func,
}

module.exports = connect(mapStateToProps, mapDispatchToProps)(AccountDetailsModal)

// Not yet pixel perfect todos:
  // fonts of qr-header

AccountDetailsModal.prototype.render = function () {
  const {
    token: {
      address,
      symbol,
      url,
      className,
      network,
      image,
    },
  } = this.props

  return h(AccountModalContainer, { hideIdenticon: true }, [
      h(Identicon, {
        className,
        address,
        network,
        image,
        diameter: 64,
      }),
      h('div', {style: {'fontWeight': 'bold', 'fontSize': '2em', 'paddingTop': '5px'}}, [
        symbol,
      ]),
      h(QrView, {
        Qr: {
          data: address,
        },
      }),

      h('div.account-modal-divider'),

      h(Button, {
        type: 'primary',
        className: 'account-modal__button',
        onClick: () => global.platform.openWindow({ url }),
      }, this.context.t('viewOnExplorer')),

  ])
}
