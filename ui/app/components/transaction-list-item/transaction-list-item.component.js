import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Identicon from '../identicon'
import TransactionStatus from '../transaction-status'
import TransactionAction from '../transaction-action'
import TokenCurrencyDisplay from '../token-currency-display'
import TransactionListItemDetails from '../transaction-list-item-details'
import { CONFIRM_TRANSACTION_ROUTE } from '../../routes'
import { UNAPPROVED_STATUS, TOKEN_METHOD_TRANSFER } from '../../constants/transactions'
import { NAKA } from '../../constants/common'
import { getStatusKey, getTokenData } from '../../helpers/transactions.util'
import { calcTokenAmount, getTokenValue } from '../../token-util'
import { hexToDecimal } from '../../helpers/conversions.util'

export default class TransactionListItem extends PureComponent {
  static propTypes = {
    assetImages: PropTypes.object,
    history: PropTypes.object,
    methodData: PropTypes.object,
    nonceAndDate: PropTypes.string,
    primaryTransaction: PropTypes.object,
    retryTransaction: PropTypes.func,
    setSelectedToken: PropTypes.func,
    showCancelModal: PropTypes.func,
    showCancel: PropTypes.bool,
    showRetry: PropTypes.bool,
    token: PropTypes.object,
    tokenData: PropTypes.object,
    transaction: PropTypes.object,
    transactionGroup: PropTypes.object,
    value: PropTypes.string,
    fetchBasicGasAndTimeEstimates: PropTypes.func,
    fetchGasEstimates: PropTypes.func,
    selectedAddress: PropTypes.string,
  }

  state = {
    showTransactionDetails: false,
  }

  getNAKATransfers () {
    const { transactionGroup:
      { primaryTransaction, primaryTransaction:
        { txParams: { to, from, value, exchanger, token, exchangeRate, gas, gasPrice } },
      },
      selectedAddress,
    } = this.props
    const decimals = '18'
    const displayValues = {}
    // calculate the tx amount if any
    if (primaryTransaction) {
      // if data === 0x, then it's tx using this token as the gas fee, no data decoding
      const tokenValue = hexToDecimal(value)
      if (tokenValue > 0) {
        const toUser = to
        const fromUser = from
        const valueStr = calcTokenAmount(tokenValue, decimals).toString()
        if (toUser === selectedAddress) {
          displayValues.nakaReceived = {
            display: `+${valueStr}`,
            symbol: NAKA,
          }
        }
        if (fromUser === selectedAddress) {
          displayValues.nakaSent = {
            display: `-${valueStr}`,
            symbol: NAKA,
          }
        }
      }
      // calculate naka fee if tokenTransfer is empty and no exchanger
      if (from === selectedAddress &&
        (!exchanger ||
          !token ||
          exchangeRate === 0)) {
            const gasFee = hexToDecimal(gas) * hexToDecimal(gasPrice)
            const gasFeeStr = calcTokenAmount(gasFee, decimals).toString().substring(0, 8)
            displayValues.gasFeeSent = {
              display: `-${gasFeeStr}`,
              symbol: NAKA,
            }
          }
    }

    // calculate fees if any
    this.calcFee(primaryTransaction.tokenTransfers, displayValues)
    // calculate other token amount if any
    this.calcAmount(primaryTransaction.tokenTransfers, displayValues)

    return displayValues
  }

  calcFee (transactions, displayValues, token = null) {
    const { selectedAddress} = this.props
    if (!transactions || transactions.length === 0) return

    if (transactions[0].txParams) {
      // filter out pay by token txs
      const feeTxs = transactions.filter(tx => tx.txParams.payByToken === true)
      if (feeTxs.length === 0) return

      const { decimals = '' } = token

      // extract payment and refund fees
      let fee1 = feeTxs.length > 0 ? hexToDecimal(feeTxs[0].txParams.value) : 0
      let fee2 = feeTxs.length > 1 ? hexToDecimal(feeTxs[1].txParams.value) : 0
      fee1 = calcTokenAmount(fee1, decimals)
      fee2 = calcTokenAmount(fee2, decimals)

      // determine which tx is the payment and which is the refund
      const payment = fee1 >= fee2 ? 0 : 1
      const refund = fee1 > fee2 ? 1 : 0

      const exchanger = feeTxs.length === 2 ? feeTxs[refund].txParams.from : feeTxs.length === 1 ? feeTxs[payment].txParams.toUser : undefined

      // calculate the gas fee if any
      const gasFeeStr = Math.abs(fee1 - fee2).toString().substring(0, 8)

      if (feeTxs[payment].txParams.from === selectedAddress) {
        // this account sent total for fee
        displayValues.gasFeeSent = {
          display: `-${gasFeeStr}`,
          symbol: token.symbol,
        }
      }
      if (selectedAddress === exchanger && feeTxs[payment].txParams.toUser === selectedAddress) {
        // this account receive this fee
        displayValues.gasFeeReceived = {
          display: `+${gasFeeStr}`,
          symbol: token.symbol,
        }
      }
    } else {
      // filter out pay by txlist txs
      const feeTxs = transactions.filter(tx => tx.payByToken === true)
      if (feeTxs.length === 0) return

      token = feeTxs[0].token
      const { decimals = '' } = token

      // extract payment and refund fees
      let fee1 = feeTxs.length > 0 ? feeTxs[0].value : 0
      let fee2 = feeTxs.length > 1 ? feeTxs[1].value : 0
      fee1 = calcTokenAmount(fee1, decimals)
      fee2 = calcTokenAmount(fee2, decimals)

      // determine which tx is the payment and which is the refund
      const payment = fee1 >= fee2 ? 0 : 1
      const refund = fee1 > fee2 ? 1 : 0
      const exchanger = feeTxs.length === 2 ? feeTxs[refund].from : feeTxs.length === 1 ? feeTxs[payment].to : undefined

      // calculate the gas fee if any
      const gasFeeStr = Math.abs(fee1 - fee2).toString().substring(0, 8)
      // feeTxs are from txlist tokenTransfers
      if (feeTxs[payment].from === selectedAddress) {
        // this account sent total for fee
        displayValues.gasFeeSent = {
          display: `-${gasFeeStr}`,
          symbol: token.symbol,
        }
      }
      if (selectedAddress === exchanger && feeTxs[payment].to === selectedAddress) {
        // this account receive this fee
        displayValues.gasFeeReceived = {
          display: `+${gasFeeStr}`,
          symbol: token.symbol,
        }
      }
    }
  }

  calcAmount (transactions, displayValues, token = null) {
    const { selectedAddress} = this.props
    if (!transactions || transactions.length === 0) return

    if (transactions[0].txParams) {
      // calculate the tx amount if any
      const txs = transactions.filter(tx => tx.txParams.payByToken === false)
      if (txs.length === 0) return

      const primaryTx = txs[0]
      const { decimals = '' } = token

      const tokenValue = hexToDecimal(primaryTx.txParams.value)
      const toUser = primaryTx.txParams.toUser
      const fromUser = primaryTx.txParams.from
      const valueStr = calcTokenAmount(tokenValue, decimals).toString()
      if (toUser === selectedAddress) {
        displayValues.tokenReceived = {
          display: `+${valueStr}`,
          symbol: token.symbol,
        }
      }
      if (fromUser === selectedAddress) {
        displayValues.tokenSent = {
          display: `-${valueStr}`,
          symbol: token.symbol,
        }
      }
    } else {
      // from txlist
      // calculate the tx amount if any
      const txs = transactions.filter(tx => tx.payByToken === false)
      if (txs.length === 0) return

      const primaryTx = txs[0]
      token = primaryTx.token
      const { decimals = '' } = token

      const toUser = primaryTx.to
      const fromUser = primaryTx.from
      const valueStr = calcTokenAmount(primaryTx.value, decimals).toString()
      if (toUser === selectedAddress) {
        displayValues.tokenReceived = {
          display: `+${valueStr}`,
          symbol: token.symbol,
        }
      }
      if (fromUser === selectedAddress) {
        displayValues.tokenSent = {
          display: `-${valueStr}`,
          symbol: token.symbol,
        }
      }
    }
  }

  getTokenTransfers () {
    const { token, transactionGroup: { transactions } } = this.props
    const displayValues = {}

    this.calcFee(transactions, displayValues, token)
    this.calcAmount(transactions, displayValues, token)

    return displayValues
  }

  getPendingTransfers () {
    const { token, primaryTransaction: { txParams: { data } = {} } = {} } = this.props
    const displayValues = {}
    if (!token) return displayValues
    const { decimals = '' } = token
    const tokenData = getTokenData(data)

    if (!tokenData) {
      return displayValues
    }

    if (tokenData.params && tokenData.params.length) {
      const tokenValue = getTokenValue(tokenData.params)
      displayValues.tokenSent = {
        display: `-${calcTokenAmount(tokenValue, decimals).toString()}`,
        symbol: token.symbol,
      }
    }

    return displayValues
  }

  handleClick = () => {
    const {
      transaction,
      history,
    } = this.props
    const { id, status } = transaction
    const { showTransactionDetails } = this.state

    if (status === UNAPPROVED_STATUS) {
      history.push(`${CONFIRM_TRANSACTION_ROUTE}/${id}`)
      return
    }

    this.setState({ showTransactionDetails: !showTransactionDetails })
  }

  handleCancel = id => {
    const {
      primaryTransaction: { txParams: { gasPrice } } = {},
      transaction: { id: initialTransactionId },
      showCancelModal,
    } = this.props

    const cancelId = id || initialTransactionId
    showCancelModal(cancelId, gasPrice)
  }

  /**
   * @name handleRetry
   * @description Resubmits a transaction. Retrying a transaction within a list of transactions with
   * the same nonce requires keeping the original value while increasing the gas price of the latest
   * transaction.
   * @param {number} id - Transaction id
   */
  handleRetry = id => {
    const {
      primaryTransaction: { txParams: { gasPrice } } = {},
      transaction: { txParams: { to } = {}, id: initialTransactionId },
      methodData: { name } = {},
      setSelectedToken,
      retryTransaction,
      fetchBasicGasAndTimeEstimates,
      fetchGasEstimates,
    } = this.props

    if (name === TOKEN_METHOD_TRANSFER) {
      setSelectedToken(to)
    }

    const retryId = id || initialTransactionId

    return fetchBasicGasAndTimeEstimates()
      .then(basicEstimates => fetchGasEstimates(basicEstimates.blockTime))
      .then(retryTransaction(retryId, gasPrice))
  }

  renderCurrencyTransfers (displayValues) {
    return (
        <TokenCurrencyDisplay
          className="transaction-list-item__amount transaction-list-item__amount--primary"
          displayValues={displayValues}
        />
      )
  }

  render () {
    const {
      assetImages,
      transaction,
      methodData,
      nonceAndDate,
      primaryTransaction,
      showCancel,
      showRetry,
      tokenData,
      transactionGroup,
      token,
    } = this.props

    const { txParams = {} } = transaction
    const { showTransactionDetails } = this.state
    const toAddress = tokenData
      ? tokenData.params && tokenData.params[0] && tokenData.params[0].value || txParams.to
      : txParams.to

    let displayValues = token ? this.getTokenTransfers() : this.getNAKATransfers()
    if (token && Object.keys(displayValues).length === 0) {
      displayValues = this.getPendingTransfers()
    }

    return (
      <div className="transaction-list-item">
        <div
          className="transaction-list-item__grid"
          onClick={this.handleClick}
        >
          <Identicon
            className="transaction-list-item__identicon"
            address={toAddress}
            diameter={34}
            image={assetImages[toAddress]}
          />
          <TransactionAction
            transaction={transaction}
            methodData={methodData}
            displayValues={displayValues}
            token={token}
            className="transaction-list-item__action"
          />
          <div
            className="transaction-list-item__nonce"
            title={nonceAndDate}
          >
            { nonceAndDate }
          </div>
          <TransactionStatus
            className="transaction-list-item__status"
            statusKey={getStatusKey(primaryTransaction)}
            title={(
              (primaryTransaction.err && primaryTransaction.err.rpc)
                ? primaryTransaction.err.rpc.message
                : primaryTransaction.err && primaryTransaction.err.message
            )}
          />
          { this.renderCurrencyTransfers(displayValues) }
        </div>
        <div className={classnames('transaction-list-item__expander', {
          'transaction-list-item__expander--show': showTransactionDetails,
        })}>
          {
            showTransactionDetails && (
              <div className="transaction-list-item__details-container">
                <TransactionListItemDetails
                  transactionGroup={transactionGroup}
                  onRetry={this.handleRetry}
                  showRetry={showRetry && methodData.done}
                  onCancel={this.handleCancel}
                  showCancel={showCancel}
                  displayValues={displayValues}
                />
              </div>
            )
          }
        </div>
      </div>
    )
  }
}
