import React, { Component } from 'react'
import PropTypes from 'prop-types'
import PageContainerContent from '../../page-container/page-container-content.component'
import SendPrecisionRow from './send-precision-row/'
import SendSymbolRow from './send-symbol-row/'
import SendNameRow from './send-name-row/'
import SendFromRow from './send-from-row/'
import SendToRow from './send-to-row/'
import SendAmountRow from './send-amount-row/'
import SendTotalSupplyRow from './send-total-supply-row/'
import SendGasChosenRow from './send-gas-chosen-row/'
import SendHexDataRow from './send-hex-data-row'
import ManageToken from './manage-token-subpage'
import { CREATE_TOKEN, MANAGE_TOKEN, SEND } from '../send.constants'

export default class SendContent extends Component {

  static propTypes = {
    updateGas: PropTypes.func,
    scanQrCode: PropTypes.func,
    showHexData: PropTypes.bool,
    type: PropTypes.string,
  };

  updateGas = (updateData) => this.props.updateGas(updateData)

  renderCreateContent () {
    return (
      <div className="send-v2__form">
        <div className="add-token__custom-token-form">
          <SendNameRow updateGas={this.updateGas} />
          <SendSymbolRow updateGas={this.updateGas} />
          <SendPrecisionRow updateGas={this.updateGas} />
          <SendTotalSupplyRow updateGas={this.updateGas} />
          <SendGasChosenRow updateGas={this.updateGas} />
          {this.props.showHexData && <SendHexDataRow updateGas={this.updateGas} />}
        </div>
      </div>
    )
  }

  renderManageToken () {
    return (
      <div className="send-v2__form">
        <ManageToken />
        {(this.props.showHexData && (
          <SendHexDataRow
            updateGas={this.updateGas}
          />
        ))}
      </div>
    )
  }

  renderSend () {
    return (
      <div className="send-v2__form">
        <SendFromRow />
        <SendToRow
          updateGas={this.updateGas}
          scanQrCode={ _ => this.props.scanQrCode()}
        />
        <SendAmountRow updateGas={this.updateGas} />
        <SendGasChosenRow updateGas={this.updateGas}/>
        {(this.props.showHexData && (
          <SendHexDataRow
            updateGas={this.updateGas}
          />
        ))}
        </div>
    )
  }

  render () {
    const { type } = this.props
    return (
      <PageContainerContent>
        {type === CREATE_TOKEN && this.renderCreateContent()}
        {type === MANAGE_TOKEN && this.renderManageToken()}
        {type === SEND && this.renderSend()}
      </PageContainerContent>
    )
  }
}
