import { connect } from 'react-redux'

import SendNameRow from './send-name-row.component'
import { updateCreateTokenName, updateCreateTokenData } from '../../../../actions'
import { updateSendErrors } from '../../../../ducks/send.duck'

const mapStateToProps = ({ metamask, send }) => {
  const { createToken } = metamask
  const { errors: { nameError } } = send

  return {
    createToken,
    nameError,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    updateCreateTokenName: name => dispatch(updateCreateTokenName(name)),
    updateCreateTokenData: (createToken) =>
      dispatch(updateCreateTokenData(createToken)),
    updateSendErrors: (errorObject) => dispatch(updateSendErrors(errorObject)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SendNameRow)
