import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { debounce } from 'lodash'

import TextField from '../../../../../../ui/app/components/text-field'

class SendNameRow extends Component {
  static contextTypes = {
    t: PropTypes.func,
  }

  static propTypes = {
    createToken: PropTypes.object,
    nameError: PropTypes.string,
    updateCreateTokenName: PropTypes.func.isRequired,
    updateCreateTokenData: PropTypes.func.isRequired,
    updateSendErrors: PropTypes.func.isRequired,
    updateGas: PropTypes.func.isRequired,
  }

  constructor (props) {
    super(props)

    this.state = {
      name: '',
      autoFilled: false,
    }
  }

  onChangeDebounce = debounce((name) => {
    const {
      createToken,
      updateCreateTokenName,
      updateCreateTokenData,
      updateSendErrors,
      updateGas,
    } = this.props

    const nameLen = name.length
    let nameError
    if (nameLen <= 0 || nameLen >= 37) {
      nameError = this.context.t('nameBetweenOneThirtySix')
      updateSendErrors({ nameError })
    } else if (name.toLowerCase() === 'naka') {
      nameError = this.context.t('nameShouldNotBeNaka')
      updateSendErrors({ nameError })
    } else {
      updateCreateTokenName(name)
      updateCreateTokenData(createToken)
      updateSendErrors({ nameError: null })
      updateGas()
    }
  }, 300)

  onChange = (name) => {
    this.setState({ name })
    this.onChangeDebounce(name.trim())
  }

  componentWillUnmount () {
    this.props.updateSendErrors({ nameError: null })
  }

  render () {
    const { nameError } = this.props
    const { name, autoFilled } = this.state

    return (
      <TextField
        id="token-name"
        label={this.context.t('name')}
        type="text"
        value={name}
        onChange={e => this.onChange(e.target.value)}
        error={nameError}
        fullWidth
        margin="normal"
        disabled={autoFilled}
      />
    )
  }
}

export default SendNameRow
