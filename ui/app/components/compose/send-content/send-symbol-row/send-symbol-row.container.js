import { connect } from 'react-redux'

import SendSymbolRow from './send-symbol-row.component'
import { updateCreateTokenSymbol, updateCreateTokenData } from '../../../../actions'
import { updateSendErrors } from '../../../../ducks/send.duck'

const mapStateToProps = ({ metamask, send }) => {
  const { createToken } = metamask
  const { errors: { symbolError } } = send

  return {
    createToken,
    symbolError,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    updateCreateTokenSymbol: symbol => dispatch(updateCreateTokenSymbol(symbol)),
    updateCreateTokenData: (createToken) =>
      dispatch(updateCreateTokenData(createToken)),
    updateSendErrors: (errorObject) => dispatch(updateSendErrors(errorObject)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SendSymbolRow)
