import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { debounce } from 'lodash'

import TextField from '../../../../../../ui/app/components/text-field'

class SendSymbolRow extends Component {
  static contextTypes = {
    t: PropTypes.func,
  }

  static propTypes = {
    createToken: PropTypes.object,
    symbolError: PropTypes.string,
    updateCreateTokenSymbol: PropTypes.func.isRequired,
    updateCreateTokenData: PropTypes.func.isRequired,
    updateSendErrors: PropTypes.func.isRequired,
    updateGas: PropTypes.func.isRequired,
  }

  constructor (props) {
    super(props)

    this.state = {
      symbol: '',
      autoFilled: false,
    }
  }

  onChangeDebounce = debounce((symbol) => {
    const {
      createToken,
      updateCreateTokenSymbol,
      updateCreateTokenData,
      updateSendErrors,
      updateGas,
    } = this.props

    const symbolLen = symbol.length
    let symbolError = null
    if (symbolLen <= 0 || symbolLen >= 11) {
      symbolError = this.context.t('symbolBetweenOneTen')
      updateSendErrors({ symbolError })
    } else if (symbol.toLowerCase() === 'naka') {
      symbolError = this.context.t('symbolShouldNotBeNaka')
      updateSendErrors({ symbolError })
    } else {
      updateCreateTokenSymbol(symbol)
      updateCreateTokenData(createToken)
      updateSendErrors({ symbolError: null })
      updateGas()
    }
  }, 300)

  onChange = (symbol) => {
    this.setState({ symbol })
    this.onChangeDebounce(symbol.trim())
  }

  componentWillUnmount () {
    this.props.updateSendErrors({ symbolError: null })
  }

  render () {
    const { symbolError } = this.props
    const { symbol, autoFilled } = this.state

    return (
      <TextField
        id="token-symbol"
        label={this.context.t('symbol')}
        type="text"
        value={symbol}
        onChange={e => this.onChange(e.target.value)}
        error={symbolError}
        fullWidth
        margin="normal"
        disabled={autoFilled}
      />
    )
  }
}

export default SendSymbolRow
