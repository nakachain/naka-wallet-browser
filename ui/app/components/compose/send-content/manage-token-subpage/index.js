import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Typography, TextField, withStyles } from '@material-ui/core'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import ListItemText from '@material-ui/core/ListItemText'
import Divider from '@material-ui/core/Divider'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'

import styles from './styles'
import GasChosenRow from '../send-gas-chosen-row'
import SendFooter from '../../manage-send-footer'
import actions from '../../../../actions'
import { getValueFromWeiHex } from '../../../../helpers/conversions.util'
import { NAKA } from '../../../../constants/common'

class TokenManage extends Component {
  state = {
    open: false,
    changeOwnerOpen: false,
    name: '',
    totalSupply: '',
    precision: '',
    txFee: '0',
  }

  handleClickOpen = () => {
    this.setState({ open: true })
  }

  handleClickChangeOwnerOpen = () => {
    this.setState({ changeOwnerOpen: true })
  }

  handleClose = () => {
    this.setState({ open: false })
  }

  handleClickChangeOwnerClose = () => {
    this.setState({ changeOwnerOpen: false })
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    })
  }

  handleSetRateChange = event => {
    this.props.setRate(this.props.selectedTokenAddress, event.target.value)
  }

  updateGas = (updateData) => this.props.updateGas(updateData)

  render () {
    const { classes, selectedTokenAddress, payByTokens, tokens } = this.props

    const token = tokens.filter((item) => item.address === selectedTokenAddress)
    const rateValue = payByTokens && payByTokens.hasOwnProperty(selectedTokenAddress)
      ? getValueFromWeiHex({
          value: payByTokens[selectedTokenAddress].rate,
          toCurrency: NAKA,
        })
      : 0

    return (
      <div className={classes.createTokenContainer}>
        <List component="nav" disablePadding >
          <ListItem button onClick={ () => this.handleClickOpen()}>
            <ListItemText
              primary="Change Rate"
              secondary={`1 ${token[0].symbol} = ${rateValue} ${NAKA}`}
            />
            <img height={20} src="./images/caret-right.svg" />
          </ListItem>
          <Divider />
          <ListItem button onClick={ () => this.handleClickChangeOwnerOpen()}>
            <ListItemText primary="Change Owner" />
            <img height={20} src="./images/caret-right.svg" />
          </ListItem>
          <Divider />
        </List>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Change Rate</DialogTitle>
          <DialogContent classes={{root: classes.dialogContentRoot}}>
            <NewTokenField
              label="rate"
              placeholder={`${rateValue}`}
              type="number"
              startAdornment={token[0].symbol}
              endAdornment={NAKA}
              handleChange={this.handleSetRateChange} />
            <GasChosenRow updateGas={this.updateGas} />
          </DialogContent>
          <DialogActions classes={{root: classes.dialogActionRoot}}>
            <SendFooter />
          </DialogActions>
        </Dialog>
        <Dialog
          open={this.state.changeOwnerOpen}
          onClose={this.handleClickChangeOwnerClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Change Owner</DialogTitle>
          <DialogContent classes={{root: classes.dialogContentRoot}}>
            <NewTokenField
              label="Owner"
              type="string"
              handleChange={(e) => this.props.transferOwner(this.props.selectedTokenAddress, e.target.value)}/>
            <GasChosenRow updateGas={this.updateGas} />
          </DialogContent>
          <DialogActions classes={{root: classes.dialogActionRoot}}>
            <SendFooter chaneOwner/>
          </DialogActions>
        </Dialog>
      </div>
     )
  }
   static contextTypes = {
    t: PropTypes.func,
  }
}


const mapDispatchToProps = dispatch => ({
  getExchangeRates: (tokenAddress) =>
    dispatch(actions.getExchangeRates(tokenAddress)),
  setRate: (tokenAddress, exchangeRate) =>
    dispatch(actions.setRate(tokenAddress, exchangeRate)),
  updateSendAmount: (amount) => dispatch(actions.updateSendAmount(amount)),
  transferOwner: (tokenAddress, ownerAddress) =>
    dispatch(actions.transferOwner(tokenAddress, ownerAddress)),
})

function mapStateToProps (state) {
  return {
    payByTokens: state.metamask.payByTokens,
    gasTotal: state.metamask.send.gasTotal,
    selectedTokenAddress: state.metamask.selectedTokenAddress,
    tokens: state.metamask.tokens,
  }
}


const NewTokenField = withStyles(styles)(({ classes, placeholder, type, label, startAdornment, endAdornment, handleChange, ...props }) => {
  const start = startAdornment &&
    (
      <Typography className={classes.startFieldTextAdornment}>
        1 {startAdornment} =
      </Typography>
    )
  const end = endAdornment &&
    (
      <Typography className={classes.fieldTextAdornment}>
        {endAdornment}
      </Typography>
    )
    return (
      <TextField
        placeholder={placeholder}
        label={label}
        id="bootstrap-input"
        type={type}
        required
        classes= {{root: classes.textField}}
        InputProps={{
          disableUnderline: true,
          classes: {
            root: classes.bootstrapRoot,
            input: classes.bootstrapInput,
          },
          startAdornment: start,
          endAdornment: end,
        }}
        InputLabelProps={{
          shrink: true,
          className: classes.bootstrapFormLabel,
        }}
        onChange={handleChange}
      />
    )
  })


module.exports = connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(TokenManage))
