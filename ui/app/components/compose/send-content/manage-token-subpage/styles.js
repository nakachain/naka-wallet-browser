const styles = (theme) => ({
  fieldTextAdornment: {
    position: 'absolute',
    right: '30px',
    padding: '6px 0 7px',
    top: '4px',
    color: 'grey',
  },
  buttonLabel: {
    position: 'absolute',
    left: '12px',
    width: 'auto',
  },
  startFieldTextAdornment: {
    padding: '6px 0 7px',
    margin: 'auto',
    minWidth: '64px',
  },
bootstrapRoot: {
  'label + &': {
    marginTop: theme.spacing.unit * 3,
  },
  width: '100%',
},
dialogContentRoot: {
  padding: '0px 24px',
},
dialogActionRoot: {
  padding: '8px 20px 16px 20px',
},
textField: {
  marginBottom: '15px',
},
createTokenContainer: {
  textAlign: 'center',
  width: '100%',
},
gradientButton: {
  color: 'white',
  backgroundImage: 'linear-gradient(#5539DF, #8E6BF1)',
},
 bootstrapInput: {
  borderRadius: 4,
  backgroundColor: theme.palette.common.white,
  border: '1px solid #ced4da',
  fontSize: 16,
  padding: '10px 12px',
  transition: theme.transitions.create(['border-color', 'box-shadow']),
  fontFamily: 'Montserrat',
  '&:after': {
    content: '213123141jr23ihri3h',
  },
   '&:focus': {
    borderColor: '#80bdff',
    boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
  },
},
smallerExtendButton: {
  height: 32,
},
dialogTitleRoot: {
  padding: '24px 24px 0px',
},
dialogContentRoot: {
  padding: '0px 24px',
},
bootstrapFormLabel: {
  fontSize: 18,
  fontWeight: 'bold',
  fontFamily: 'Montserrat',
  textTransform: 'capitalize',
},
})
 export default styles
