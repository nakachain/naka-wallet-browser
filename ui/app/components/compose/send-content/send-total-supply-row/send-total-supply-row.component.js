import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { debounce } from 'lodash'

import TextField from '../../../../../../ui/app/components/text-field'

class SendTotalSupplyRow extends Component {
  static contextTypes = {
    t: PropTypes.func,
  }

  static propTypes = {
    createToken: PropTypes.object,
    totalSupplyError: PropTypes.string,
    updateCreateTokenTotalSupply: PropTypes.func.isRequired,
    updateCreateTokenData: PropTypes.func.isRequired,
    updateSendErrors: PropTypes.func.isRequired,
    updateGas: PropTypes.func.isRequired,
  }

  constructor (props) {
    super(props)

    this.state = {
      totalSupply: '',
      autoFilled: false,
    }
  }

  onChangeDebounce = debounce((totalSupply) => {
    const {
      createToken,
      updateCreateTokenTotalSupply,
      updateCreateTokenData,
      updateSendErrors,
      updateGas,
    } = this.props

    const totalSupplyNum = parseInt(Number(totalSupply) || 0, 10)
    let totalSupplyError = null
    if (totalSupplyNum <= 0) {
      totalSupplyError = this.context.t('totalSupplyGreaterThanZero')
      updateSendErrors({ totalSupplyError })
    } else {
      updateCreateTokenTotalSupply(totalSupply.toString())
      updateCreateTokenData(createToken)
      updateSendErrors({ totalSupplyError: null })
      updateGas()
    }
  }, 300)

  onChange = (totalSupply) => {
    this.setState({ totalSupply })
    this.onChangeDebounce(totalSupply.trim())
  }

  componentWillUnmount () {
    this.props.updateSendErrors({ totalSupplyError: null })
  }

  render () {
    const { totalSupplyError } = this.props
    const { totalSupply, autoFilled } = this.state

    return (
      <TextField
        id="token-total-supply"
        label={this.context.t('totalSupply')}
        type="text"
        value={totalSupply}
        onChange={e => this.onChange(e.target.value)}
        error={totalSupplyError}
        fullWidth
        margin="normal"
        disabled={autoFilled}
      />
    )
  }
}

export default SendTotalSupplyRow
