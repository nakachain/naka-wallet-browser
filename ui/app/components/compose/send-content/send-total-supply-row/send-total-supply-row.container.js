import { connect } from 'react-redux'

import SendTotalSupplyRow from './send-total-supply-row.component'
import {
  updateCreateTokenTotalSupply,
  updateCreateTokenData,
} from '../../../../actions'
import { updateSendErrors } from '../../../../ducks/send.duck'

const mapStateToProps = ({ metamask, send }) => {
  const { createToken } = metamask
  const { errors: { totalSupplyError } } = send
  return {
    createToken,
    totalSupplyError,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    updateCreateTokenTotalSupply: totalSupply =>
      dispatch(updateCreateTokenTotalSupply(totalSupply)),
    updateCreateTokenData: (createToken) =>
      dispatch(updateCreateTokenData(createToken)),
    updateSendErrors: (errorObject) => dispatch(updateSendErrors(errorObject)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SendTotalSupplyRow)
