const selectors = {
  gasFeeIsInError,
}

module.exports = selectors

function gasFeeIsInError (state) {
  return Boolean(state.send.errors.gasFee)
}
