import { connect } from 'react-redux'
import { setPayToken } from '../../../../actions'
import { getGasLimit, getSendHexData } from '../../send.selectors'
import { gasFeeIsInError } from './send-gas-chosen-row.selectors.js'
import SendGasChosenRow from './send-gas-chosen-row.component'

function mapStateToProps (state) {
  return {
    gasLimit: getGasLimit(state),
    gasFeeError: gasFeeIsInError(state),
    data: getSendHexData(state),
    payByTokens: state.metamask.payByTokens,
  }
}

function mapDispatchToProps (dispatch) {
  return {
    setPayToken: (tokenAddress, exchanger, rate) =>
      dispatch(setPayToken(tokenAddress, exchanger, rate)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SendGasChosenRow)
