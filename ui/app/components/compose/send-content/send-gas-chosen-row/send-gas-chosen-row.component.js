import React, { Component } from 'react'
import PropTypes from 'prop-types'
import SendRowWrapper from '../send-row-wrapper'
import { withStyles } from '@material-ui/core/styles'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import MenuItem from '@material-ui/core/MenuItem'
import Select from '@material-ui/core/Select'
import { getNativeGasFee, getTokenGasFee } from '../../../../helpers/conversions.util'

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
})


class SendHexDataRow extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    gasLimit: PropTypes.string,
    gasFeeError: PropTypes.bool,
    data: PropTypes.string,
    payByTokens: PropTypes.object,
    setPayToken: PropTypes.func.isRequired,
    updateGas: PropTypes.func.isRequired,
  }

  static contextTypes = {
    t: PropTypes.func,
  }

  state = {
    token: '',
    labelWidth: 0,
    items: [],
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
    this._setPayToken(event.target.value)
    this.props.updateGas()
  };

  getList = ({ payByTokens, gasLimit, selectedTokenAddress }) => {
    if (!gasLimit) return []
    let ret = []
    // add NAKA
    const nakaToken = getNativeGasFee(gasLimit)
    for (const payByTokenAddress in payByTokens) {
      const cur = getTokenGasFee(payByTokens[payByTokenAddress], gasLimit)
      if (cur === null) continue
      ret.push(cur)
    }
    if (selectedTokenAddress) {
      const withoutSelectedToken = ret.filter(item => item.address !== selectedTokenAddress.address)
      const selectedToken = ret.filter(item => item.address === selectedTokenAddress.address)
      ret = [...selectedToken, ...withoutSelectedToken, nakaToken]
    } else {
      ret = [nakaToken, ...ret]
    }

    this.setState({token: ret[0].address})
    this._setPayToken(ret[0].address)
    this.props.updateGas()
    return ret
  }

  _setPayToken = (tokenAddress) => {
    const { payByTokens } = this.props
    const exchanger = tokenAddress === '' ? undefined : payByTokens[tokenAddress].exchangerAddress
    const rate = tokenAddress === '' ? undefined : payByTokens[tokenAddress].rate
    this.props.setPayToken(tokenAddress, exchanger, rate)
  }

  componentDidUpdate (prevProps, prevState) {
    if (prevProps.payByTokens !== this.props.payByTokens || prevProps.gasLimit !== this.props.gasLimit) {
      const items = this.getList(this.props)
      this.setState({items})
    }
  }

  render () {
    const { classes, gasFeeError } = this.props
    const {t} = this.context
    const { items } = this.state
    return (
      <SendRowWrapper
        label={`${t('gasFee')}:`}
        showError={gasFeeError}
        errorType={'gasFee'}
      >
        <Select
          value={this.state.token}
          onChange={this.handleChange}
          displayEmpty
          className={classes.selectEmpty}
          input={
            <OutlinedInput
              labelWidth={this.state.labelWidth}
              name="token"
              fullWidth
            />
          }
        >
          {
            items.map(item => {
              return <MenuItem key={item.address} value={item.address}>{`${item.gasFee} ${item.symbol}`}</MenuItem>
            })
          }
        </Select>
      </SendRowWrapper>
    )
  }
}

export default withStyles(styles)(SendHexDataRow)

