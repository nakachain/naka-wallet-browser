import { connect } from 'react-redux'

import SendPrecisionRow from './send-precision-row.component'
import { updateCreateTokenPrecision, updateCreateTokenData } from '../../../../actions'
import { updateSendErrors } from '../../../../ducks/send.duck'

const mapStateToProps = ({ metamask, send }) => {
  const { createToken } = metamask
  const { errors: { precisionError } } = send

  return {
    createToken,
    precisionError,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    updateCreateTokenPrecision: precision =>
      dispatch(updateCreateTokenPrecision(precision)),
    updateCreateTokenData: (createToken) =>
      dispatch(updateCreateTokenData(createToken)),
    updateSendErrors: (errorObject) => dispatch(updateSendErrors(errorObject)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SendPrecisionRow)
