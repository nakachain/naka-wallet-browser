import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { debounce } from 'lodash'

import TextField from '../../../../../../ui/app/components/text-field'

class SendPrecisionRow extends Component {
  static contextTypes = {
    t: PropTypes.func,
  }

  static propTypes = {
    createToken: PropTypes.object,
    precisionError: PropTypes.string,
    updateCreateTokenPrecision: PropTypes.func.isRequired,
    updateCreateTokenData: PropTypes.func.isRequired,
    updateSendErrors: PropTypes.func.isRequired,
    updateGas: PropTypes.func.isRequired,
  }

  constructor (props) {
    super(props)

    this.state = {
      autoFilled: false,
      precision: '18',
    }
  }

  onChangeDebounce = debounce((precision) => {
    const {
      createToken,
      updateCreateTokenPrecision,
      updateCreateTokenData,
      updateSendErrors,
      updateGas,
    } = this.props

    const precisionNum = parseInt(Number(precision) || 0, 10)
    let precisionError
    if (precisionNum <= 0 || precisionNum > 18) {
      precisionError = this.context.t('decimalsMustZerotoEighteen')
      updateSendErrors({ precisionError })
    } else {
      updateCreateTokenPrecision(precisionNum.toString())
      updateCreateTokenData(createToken)
      updateSendErrors({ precisionError: null })
      updateGas()
    }
  }, 300)

  onChange = (precision) => {
    this.setState({ precision })
    this.onChangeDebounce(precision.trim())
  }

  componentDidMount () {
    this.props.updateCreateTokenPrecision('18')
  }

  componentWillUnmount () {
    this.props.updateSendErrors({ precisionError: null })
  }

  render () {
    const { precisionError } = this.props
    const { precision, autoFilled } = this.state

    return (
      <TextField
        id="token-decimals"
        label={this.context.t('decimal')}
        type="text"
        value={precision}
        onChange={e => this.onChange(e.target.value)}
        error={precisionError}
        fullWidth
        margin="normal"
        disabled={autoFilled}
      />
    )
  }
}

export default SendPrecisionRow
