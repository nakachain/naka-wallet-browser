const ethAbi = require('ethereumjs-abi')
const ethUtil = require('ethereumjs-util')
const { TOKEN_TRANSFER_FUNCTION_SIGNATURE } = require('../send.constants')

function addHexPrefixToObjectValues (obj) {
  return Object.keys(obj).reduce((newObj, key) => {
    return { ...newObj, [key]: ethUtil.addHexPrefix(obj[key]) }
  }, {})
}

function constructTxParams ({ selectedToken, data, token, exchanger, exchangeRate, to, amount, from, gas, gasPrice }) {
  const txParams = {
    data,
    token,
    exchanger,
    exchangeRate,
    from,
    value: '0',
    gas,
    gasPrice,
    to,
  }

  if (!selectedToken) {
    txParams.value = amount
    txParams.to = to
  }

  return addHexPrefixToObjectValues(txParams)
}

function constructUpdatedTx ({
  amount,
  data,
  token,
  exchanger,
  exchangeRate,
  editingTransactionId,
  from,
  gas,
  gasPrice,
  selectedToken,
  to,
  unapprovedTxs,
}) {
  const unapprovedTx = unapprovedTxs[editingTransactionId]
  const txParamsData = unapprovedTx.txParams.data ? unapprovedTx.txParams.data : data
  const txParamsToken = unapprovedTx.txParams.token ? unapprovedTx.txParams.token : token
  const txParamsExchanger = unapprovedTx.txParams.exchanger ? unapprovedTx.txParams.exchanger : exchanger
  const txParamsExchangeRate = unapprovedTx.txParams.exchangeRate ? unapprovedTx.txParams.exchangeRate : exchangeRate
  const editingTx = {
    ...unapprovedTx,
    txParams: Object.assign(
      unapprovedTx.txParams,
      addHexPrefixToObjectValues({
        data: txParamsData,
        token: txParamsToken,
        exchanger: txParamsExchanger,
        exchangeRate: txParamsExchangeRate,
        to,
        from,
        gas,
        gasPrice,
        value: amount,
      })
    ),
  }

  if (selectedToken) { // TT1943
    const data = TOKEN_TRANSFER_FUNCTION_SIGNATURE + Array.prototype.map.call(
      ethAbi.rawEncode(['address', 'uint256'], [to, ethUtil.addHexPrefix(amount)]),
      x => ('00' + x.toString(16)).slice(-2)
    ).join('')

    Object.assign(editingTx.txParams, addHexPrefixToObjectValues({
      value: '0',
      to: selectedToken.address,
      data,
    }))
  }

  if (typeof editingTx.txParams.data === 'undefined') {
    delete editingTx.txParams.data
  }

  if (typeof editingTx.txParams.token === 'undefined') {
    delete editingTx.txParams.token
  }

  if (typeof editingTx.txParams.exchanger === 'undefined') {
    delete editingTx.txParams.exchanger
  }

  if (typeof editingTx.txParams.exchangeRate === 'undefined') {
    delete editingTx.txParams.exchangeRate
  }

  return editingTx
}

function addressIsNew (toAccounts, newAddress) {
  return !toAccounts.find(({ address }) => newAddress === address)
}

module.exports = {
  addressIsNew,
  constructTxParams,
  constructUpdatedTx,
  addHexPrefixToObjectValues,
}
