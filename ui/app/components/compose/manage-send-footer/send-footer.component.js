import React, { Component } from 'react'
import PropTypes from 'prop-types'
const h = require('react-hyperscript')
import { withRouter } from 'react-router-dom'
import PageContainerFooter from '../../page-container/page-container-footer'
import { CONFIRM_TRANSACTION_ROUTE, DEFAULT_ROUTE } from '../../../routes'
import Button from '../../button'
class SendFooter extends Component {

  static propTypes = {
    addToAddressBookIfNew: PropTypes.func,
    amount: PropTypes.string,
    data: PropTypes.string,
    token: PropTypes.string,
    exchanger: PropTypes.string,
    exchangeRate: PropTypes.string,
    clearSend: PropTypes.func,
    disabled: PropTypes.bool,
    editingTransactionId: PropTypes.string,
    errors: PropTypes.object,
    from: PropTypes.object,
    gasLimit: PropTypes.string,
    gasPrice: PropTypes.string,
    gasTotal: PropTypes.string,
    history: PropTypes.object,
    inError: PropTypes.bool,
    selectedToken: PropTypes.object,
    sign: PropTypes.func,
    to: PropTypes.string,
    toAccounts: PropTypes.array,
    tokenBalance: PropTypes.string,
    unapprovedTxs: PropTypes.object,
    update: PropTypes.func,
  }

  static contextTypes = {
    t: PropTypes.func,
  }

  onCancel () {
    this.props.clearSend()
    this.props.history.push(DEFAULT_ROUTE)
  }

  onSubmit (event) {
    event.preventDefault()

    const {
      addToAddressBookIfNew,
      amount,
      data,
      token,
      exchanger,
      exchangeRate,
      editingTransactionId,
      from: {address: from},
      gasLimit: gas,
      gasPrice,
      selectedToken,
      sign,
      to,
      unapprovedTxs,
      // updateTx,
      update,
      toAccounts,
      history,
    } = this.props

    // Should not be needed because submit should be disabled if there are errors.
    // const noErrors = !amountError && toError === null

    // if (!noErrors) {
    //   return
    // }

    // TODO: add nickname functionality
    addToAddressBookIfNew(to, toAccounts)
    const promise = editingTransactionId
      ? update({
        amount,
        data,
        token,
        exchanger,
        exchangeRate,
        editingTransactionId,
        from,
        gas,
        gasPrice,
        selectedToken,
        to,
        unapprovedTxs,
      })
      : sign({ data, token, exchanger, exchangeRate, selectedToken, to, amount, from, gas, gasPrice })

    Promise.resolve(promise)
      .then(() => history.push(CONFIRM_TRANSACTION_ROUTE))
  }

  formShouldBeDisabled () {
    const { data, inError, selectedToken, tokenBalance, gasTotal, to } = this.props
    const missingTokenBalance = selectedToken && !tokenBalance

    return inError || !gasTotal || missingTokenBalance || !(data || to)
  }

  componentDidMount () {
    const { tokenAddresses, getExchangeRates } = this.props
    getExchangeRates(tokenAddresses)
  }

  render () {
    const dis = this.formShouldBeDisabled()
    return (
      h(Button, {
        type: 'primary',
        className: 'account-modal__button',
        onClick: (e) => this.onSubmit(e),
        disabled: dis,
      }, this.context.t('confirm'))
    )
  }

}

export default withRouter(SendFooter)
