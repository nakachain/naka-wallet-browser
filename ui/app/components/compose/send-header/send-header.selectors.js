const {
  getSelectedToken,
  getSendEditingTransactionId,
} = require('../send.selectors.js')

const selectors = {
  getTitleKey,
  getSubtitleParams,
}

function getTitleKey (state, ownProps) {
  const isEditing = Boolean(getSendEditingTransactionId(state))
  const isToken = Boolean(getSelectedToken(state))
  const { type } = ownProps
  if (isEditing) {
    return 'edit'
  } else {
    if (type === 'createToken') {
      return 'createToken'
    } else if (type === 'send') {
      if (isToken) {
        return 'sendTokens'
      } else {
        return 'sendNAKA'
      }
    } else if (type === 'manageToken') {
      return 'manageToken'
    }
  }
}

function getSubtitleParams (state, ownProps) {
  const isEditing = Boolean(getSendEditingTransactionId(state))
  const token = getSelectedToken(state)
  const { type } = ownProps
  if (isEditing) {
    return [ 'editingTransaction' ]
  } else {
    if (type === 'createToken') {
      return [ 'createANewNRCToken' ]
    } else if (type === 'send') {
      if (token) {
        return [ 'onlySendTokensToAccountAddress', [ token.symbol ] ]
      } else {
        return [ 'onlySendNakaToAddress' ]
      }
    } else if (type === 'manageToken') {
      return [ 'manageXToken', [ token.symbol ] ]
    }
  }
}

module.exports = selectors
