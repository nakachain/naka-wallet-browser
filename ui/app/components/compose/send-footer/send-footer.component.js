import React, { Component } from 'react'
import PropTypes from 'prop-types'

import PageContainerFooter from '../../page-container/page-container-footer'
import { CONFIRM_TRANSACTION_ROUTE, DEFAULT_ROUTE } from '../../../routes'
import { SEND, CREATE_TOKEN } from '../../compose/send.constants'

export default class SendFooter extends Component {

  static propTypes = {
    type: PropTypes.string,
    addToAddressBookIfNew: PropTypes.func,
    amount: PropTypes.string,
    data: PropTypes.string,
    token: PropTypes.string,
    exchanger: PropTypes.string,
    exchangeRate: PropTypes.string,
    clearSend: PropTypes.func,
    disabled: PropTypes.bool,
    editingTransactionId: PropTypes.string,
    errors: PropTypes.object,
    from: PropTypes.object,
    gasLimit: PropTypes.string,
    gasPrice: PropTypes.string,
    gasTotal: PropTypes.string,
    history: PropTypes.object,
    inError: PropTypes.bool,
    selectedToken: PropTypes.object,
    sign: PropTypes.func,
    to: PropTypes.string,
    toAccounts: PropTypes.array,
    tokenBalance: PropTypes.string,
    unapprovedTxs: PropTypes.object,
    update: PropTypes.func,
    updateSendTo: PropTypes.func,
    createToken: PropTypes.object,
  }

  static contextTypes = {
    t: PropTypes.func,
  }

  onCancel () {
    this.props.clearSend()
    this.props.history.push(DEFAULT_ROUTE)
  }

  onSubmit (event) {
    event.preventDefault()

    const {
      addToAddressBookIfNew,
      amount,
      data,
      token,
      exchanger,
      exchangeRate,
      editingTransactionId,
      from: { address: from },
      gasLimit: gas,
      gasPrice,
      selectedToken,
      sign,
      to,
      unapprovedTxs,
      update,
      toAccounts,
      history,
      type,
    } = this.props

    // Should not be needed because submit should be disabled if there are errors.
    // const noErrors = !amountError && toError === null

    // if (!noErrors) {
    //   return
    // }

    // TODO: add nickname functionality
    addToAddressBookIfNew(to, toAccounts)

    // Remove the to field when creating tokens
    if (type === CREATE_TOKEN) this.props.updateSendTo()

    const promise = editingTransactionId
      ? update({
          amount,
          data,
          token,
          exchanger,
          exchangeRate,
          editingTransactionId,
          from,
          gas,
          gasPrice,
          selectedToken,
          to,
          unapprovedTxs,
        })
      : sign({
          data,
          token,
          exchanger,
          exchangeRate,
          selectedToken,
          to,
          amount,
          from,
          gas,
          gasPrice,
        })

    Promise.resolve(promise)
      .then(() => history.push(CONFIRM_TRANSACTION_ROUTE))
  }

  createFormShouldBeDisabled () {
    const {
      inError,
      gasTotal,
      selectedToken,
      tokenBalance,
      createToken: {
        name,
        symbol,
        precision,
        totalSupply,
      },
    } = this.props

    return inError ||
      !gasTotal ||
      (selectedToken && !tokenBalance) ||
      !(name && symbol && precision && totalSupply)
  }

  sendFormShouldBeDisabled () {
    const { data, inError, selectedToken, tokenBalance, gasTotal, to } = this.props
    const missingTokenBalance = selectedToken && !tokenBalance
    return inError || !gasTotal || missingTokenBalance || !(data || to)
  }

  formShouldBeDisabled () {
    const { type } = this.props
    switch (type) {
      case CREATE_TOKEN:
        return this.createFormShouldBeDisabled()
      case SEND:
        return this.sendFormShouldBeDisabled()
      default:
        return false
    }
  }

  componentDidMount () {
    const { tokenAddresses, getExchangeRates } = this.props
    getExchangeRates(tokenAddresses)
  }

  render () {
    return (
      <PageContainerFooter
        onCancel={() => this.onCancel()}
        onSubmit={e => this.onSubmit(e)}
        disabled={this.formShouldBeDisabled()}
      />
    )
  }
}
