import React from 'react'
import PropTypes from 'prop-types'

const BackButton = ({ onClick }) => {
  return (
    <div className="back-button__container">
      <a
        onClick={onClick}
        href="#"
      >
        <button className="back-button__button" type="button">
          <span>{'< Back'}</span>
        </button>
      </a>
    </div>
  )
}

BackButton.propTypes = {
  onClick: PropTypes.function,
}

export default BackButton
