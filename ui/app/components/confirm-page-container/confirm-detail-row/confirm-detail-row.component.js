import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import UserPreferencedCurrencyDisplay from '../../user-preferenced-currency-display'
import { PRIMARY } from '../../../constants/common'

const ConfirmDetailRow = props => {
  const {
    label,
    primaryText,
    onHeaderClick,
    primaryValueTextColor,
    headerText,
    headerTextClassName,
    value,
    showSendNakaConfirmPage,
  } = props

  return (
    <div className="confirm-detail-row">
      <div className="confirm-detail-row__label">
        { label }
      </div>
      <div className="confirm-detail-row__details">
        <div
          className={classnames('confirm-detail-row__header-text', headerTextClassName)}
          onClick={() => onHeaderClick && onHeaderClick()}
        >
          { headerText }
        </div>
        {
          primaryText
            ? (
              <div
                className="confirm-detail-row__primary"
                style={{ color: primaryValueTextColor }}
              >
                { primaryText }
              </div>
            ) : (
              <UserPreferencedCurrencyDisplay
                className="confirm-detail-row__primary"
                type={PRIMARY}
                value={value}
                showTokenSymbol
                showSendNakaConfirmPage={showSendNakaConfirmPage}
                ethLogoHeight="18"
                style={{ color: primaryValueTextColor }}
                hideLabel
              />
            )
        }
      </div>
    </div>
  )
}

ConfirmDetailRow.propTypes = {
  headerText: PropTypes.string,
  headerTextClassName: PropTypes.string,
  label: PropTypes.string,
  onHeaderClick: PropTypes.func,
  primaryValueTextColor: PropTypes.string,
  primaryText: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  secondaryText: PropTypes.string,
  value: PropTypes.string,
}

export default ConfirmDetailRow
