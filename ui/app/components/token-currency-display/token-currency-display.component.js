import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import CurrencyDisplay from '../currency-display'

export default class TokenCurrencyDisplay extends PureComponent {
  static contextTypes = {
    t: PropTypes.func,
  }

  static propTypes = {
    transactionData: PropTypes.string,
    token: PropTypes.object,
    transactions: PropTypes.array,
    selectedAddress: PropTypes.string,
    displayValues: PropTypes.object,
  }

  render () {
    const { displayValues } = this.props
      return Object.keys(displayValues).map((key) => (
        <div key={key} className="token-currency-display-amount">
          <div>
            <CurrencyDisplay
              {...this.props}
              displayValue={displayValues[key].display}
              suffix={displayValues[key].symbol}
            />
          </div>
          <div className="token-currency-display-info">
            { key === 'gasFeeSent' ? `(${this.context.t('sendAsFee')})` : key === 'gasFeeReceived' ? `(${this.context.t('receiveAsExchanger')})` : '' }
          </div>
        </div>
      )
    )
  }
}
