const inherits = require('util').inherits
const Component = require('react').Component
const h = require('react-hyperscript')

module.exports = Mascot

inherits(Mascot, Component)
function Mascot ({width = '200', height = '200'}) {
  Component.call(this)
}

Mascot.prototype.render = function () {
  // this is a bit hacky
  // the event emitter is on `this.props`
  // and we dont get that until render

  return h('#metamask-mascot-container', {
    style: { zIndex: 0 },
  })
}

Mascot.prototype.componentDidMount = function () {
  var targetDivId = 'metamask-mascot-container'
  var container = document.getElementById(targetDivId)
  var img = document.createElement('IMG')
  img.setAttribute('src', 'images/logo/naka_logo_512x512.png')
  img.setAttribute('width', '200')
  img.setAttribute('height', '200')
  container.appendChild(img)
}
