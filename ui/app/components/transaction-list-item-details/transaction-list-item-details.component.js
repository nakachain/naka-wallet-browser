import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import SenderToRecipient from '../sender-to-recipient'
import { FLAT_VARIANT } from '../sender-to-recipient/sender-to-recipient.constants'
import TransactionActivityLog from '../transaction-activity-log'
import TransactionBreakdown from '../transaction-breakdown'
import Button from '../button'
import Tooltip from '../tooltip'
import { getTxLink } from '../../../lib/explorer-link'

export default class TransactionListItemDetails extends PureComponent {
  static contextTypes = {
    t: PropTypes.func,
  }

  static propTypes = {
    onCancel: PropTypes.func,
    onRetry: PropTypes.func,
    showCancel: PropTypes.bool,
    showRetry: PropTypes.bool,
    transactionGroup: PropTypes.object,
    displayValues: PropTypes.object,
  }

  handleExplorerClick = () => {
    const { transactionGroup: { primaryTransaction } } = this.props
    const { hash, metamaskNetworkId } = primaryTransaction

    const url = getTxLink(metamaskNetworkId, hash)
    global.platform.openWindow({ url })
  }

  handleCancel = event => {
    const { transactionGroup: { initialTransaction: { id } = {} } = {}, onCancel } = this.props

    event.stopPropagation()
    onCancel(id)
  }

  handleRetry = event => {
    const { transactionGroup: { initialTransaction: { id } = {} } = {}, onRetry } = this.props

    event.stopPropagation()
    onRetry(id)
  }

  render () {
    const { t } = this.context
    const { transactionGroup, showCancel, showRetry, onCancel, onRetry, displayValues } = this.props
    const { primaryTransaction: transaction } = transactionGroup
    const { txParams: { to, from, toUser } = {} } = transaction

    return (
      <div className="transaction-list-item-details">
        <div className="transaction-list-item-details__header">
          <div>{ t('details') }</div>
          <div className="transaction-list-item-details__header-buttons">
            {
              showRetry && (
                <Button
                  type="raised"
                  onClick={this.handleRetry}
                  className="transaction-list-item-details__header-button"
                >
                  { t('speedUp') }
                </Button>
              )
            }
            {
              showCancel && (
                <Button
                  type="raised"
                  onClick={this.handleCancel}
                  className="transaction-list-item-details__header-button"
                >
                  { t('cancel') }
                </Button>
              )
            }
            <Tooltip title={t('viewOnExplorer')}>
              <Button
                type="raised"
                onClick={this.handleExplorerClick}
                className="transaction-list-item-details__header-button"
                >
                <img src="/images/arrow-popout.svg" />
              </Button>
            </Tooltip>
          </div>
        </div>
        <div className="transaction-list-item-details__body">
          <div className="transaction-list-item-details__sender-to-recipient-container">
            <SenderToRecipient
              variant={FLAT_VARIANT}
              addressOnly
              recipientAddress={to}
              senderAddress={from}
              toUserAddress={toUser}
            />
          </div>
          <div className="transaction-list-item-details__cards-container">
            <TransactionBreakdown
              transaction={transaction}
              transactionInfo={displayValues}
              className="transaction-list-item-details__transaction-breakdown"
            />
            {transaction.history && <TransactionActivityLog
              transactionGroup={transactionGroup}
              className="transaction-list-item-details__transaction-activity-log"
              onCancel={onCancel}
              onRetry={onRetry}
              transactionInfo={displayValues}
            />}
          </div>
        </div>
      </div>
    )
  }
}
