import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import MenuBar from './menu-bar.component'
import { showSidebar, hideSidebar } from '../../actions'

const mapStateToProps = state => {
  const { appState: { sidebar: { isOpen } } } = state

  return {
    sidebarOpen: isOpen,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    showSidebar: () => {
      dispatch(showSidebar({
        transitionName: 'sidebar-right',
        type: 'wallet-view',
      }))
    },
    hideSidebar: () => dispatch(hideSidebar()),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MenuBar))
