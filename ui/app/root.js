require('react').PropTypes = require('prop-types')
require('react').createClass = require('create-react-class')
const { Component } = require('react')
const PropTypes = require('prop-types')
const { HashRouter } = require('react-router-dom')
const { Provider } = require('react-redux')
const h = require('react-hyperscript')
const I18nProvider = require('./i18n-provider')
const App = require('./app')

class Root extends Component {
  render () {
    const { store } = this.props

    return (
      h(Provider, { store }, [
        h(HashRouter, { hashType: 'noslash' }, [
          h(I18nProvider, [
            h(App),
          ]),
        ]),
      ])
    )
  }
}

Root.propTypes = {
  store: PropTypes.object,
}

module.exports = Root
