import ethUtil from 'ethereumjs-util'
import { BigNumber } from 'bignumber.js'
import { isNull } from 'lodash'

import { NAKA, GWEI, WEI } from '../constants/common'
import { conversionUtil, addCurrencies } from '../conversion-util'

const GAS_LIMIT_DEFAULT = '0x5208'
const GAS_PRICE_DEFAULT = '0xb165100c4'

export function bnToHex (inputBn) {
  return ethUtil.addHexPrefix(inputBn.toString(16))
}

export function hexToDecimal (hexValue) {
  return conversionUtil(hexValue, {
    fromNumericBase: 'hex',
    toNumericBase: 'dec',
  })
}

export function decimalToHex (decimal) {
  return conversionUtil(decimal, {
    fromNumericBase: 'dec',
    toNumericBase: 'hex',
  })
}

export function getEthConversionFromWeiHex ({ value, fromCurrency = NAKA, conversionRate, numberOfDecimals = 6 }) {
  const denominations = [fromCurrency, GWEI, WEI]

  let nonZeroDenomination

  for (let i = 0; i < denominations.length; i++) {
    const convertedValue = getValueFromWeiHex({
      value,
      conversionRate,
      fromCurrency,
      toCurrency: fromCurrency,
      numberOfDecimals,
      toDenomination: denominations[i],
    })

    if (convertedValue !== '0' || i === denominations.length - 1) {
      nonZeroDenomination = `${convertedValue} ${denominations[i]}`
      break
    }
  }

  return nonZeroDenomination
}

export function getValueFromWeiHex ({
  value,
  fromCurrency = NAKA,
  toCurrency,
  conversionRate,
  numberOfDecimals,
  toDenomination,
}) {
  return conversionUtil(value, {
    fromNumericBase: 'hex',
    toNumericBase: 'dec',
    fromCurrency,
    toCurrency,
    numberOfDecimals,
    fromDenomination: WEI,
    toDenomination,
    conversionRate,
  })
}

export function getWeiHexFromDecimalValue ({
  value,
  fromCurrency,
  conversionRate,
  fromDenomination,
  invertConversionRate,
}) {
  return conversionUtil(value, {
    fromNumericBase: 'dec',
    toNumericBase: 'hex',
    toCurrency: NAKA,
    fromCurrency,
    conversionRate,
    invertConversionRate,
    fromDenomination,
    toDenomination: WEI,
  })
}

export function addHexWEIsToDec (aHexWEI, bHexWEI) {
  return addCurrencies(aHexWEI, bHexWEI, {
    aBase: 16,
    bBase: 16,
    fromDenomination: 'WEI',
    numberOfDecimals: 6,
  })
}

export function decEthToConvertedCurrency (ethTotal, convertedCurrency, conversionRate) {
  return conversionUtil(ethTotal, {
    fromNumericBase: 'dec',
    toNumericBase: 'dec',
    fromCurrency: 'NAKA',
    toCurrency: convertedCurrency,
    numberOfDecimals: 2,
    conversionRate,
  })
}

export function decGWEIToHexWEI (decGWEI) {
  return conversionUtil(decGWEI, {
    fromNumericBase: 'dec',
    toNumericBase: 'hex',
    fromDenomination: 'GWEI',
    toDenomination: 'WEI',
  })
}

export function hexWEIToDecGWEI (decGWEI) {
  return conversionUtil(decGWEI, {
    fromNumericBase: 'hex',
    toNumericBase: 'dec',
    fromDenomination: 'WEI',
    toDenomination: 'GWEI',
  })
}

/**
 * Removes the trailing zeros in a number string.
 * @param {string} str Number string to remove the trailing zeros.
 * @return {string} Number string with trailing zeros removed.
 */
export function removeTrailingZeros (str) {
  return str.replace(/\.?0+$/g, '')
}

/**
 * Returns the native gas fee and other token info.
 * gasLimit x gasPrice = nativeGasFee
 * @param {string} gasLimit Gas limit (hex) for the transaction.
 * @param {string} gasPrice Gas price (hex) for the transaction.
 * @return {object} Native gas fee (decimal) and other token info.
 */
export function getNativeGasFee (
  gasLimit = GAS_LIMIT_DEFAULT,
  gasPrice = GAS_PRICE_DEFAULT
) {
  gasLimit = isNull(gasLimit) ? GAS_LIMIT_DEFAULT : gasLimit
  gasPrice = isNull(gasPrice) ? GAS_PRICE_DEFAULT : gasPrice
  const gasFee = removeTrailingZeros(new BigNumber(gasLimit, 16)
    .mul(gasPrice)
    .div(new BigNumber(10).pow(Number(18)))
    .toFormat(18))
  return { gasFee, symbol: NAKA, address: '' }
}

/**
 * Returns the token gas fee and other token info.
 * nativeGasFee / exchangeRate = tokenGasFee (highest denomination)
 * @param {object} payByToken Pay By Token object.
 * @param {string} gasLimit Gas limit (hex) for the transaction.
 * @param {string} gasPrice Gas price (hex) for the transaction.
 * @return {object} Token gas fee (decimal) and other token info.
 */
export function getTokenGasFee (
  payByToken,
  gasLimit = GAS_LIMIT_DEFAULT,
  gasPrice = GAS_PRICE_DEFAULT
) {
  const gasFee = removeTrailingZeros(new BigNumber(gasLimit, 16)
    .mul(gasPrice)
    .div(new BigNumber(payByToken.rate, 16))
    .toFormat(payByToken.decimals || 18))
  return {
    gasFee,
    address: payByToken.tokenAddress,
    symbol: payByToken.symbol,
  }
}

/**
 * Returns the token gas fee (highest denomination).
 * nativeGasFee / exchangeRate = tokenGasFee (highest denomination)
 * @param {string} gasLimit Gas limit (hex) for the tx.
 * @param {string} gasPrice Gas price (hex) for the tx.
 * @param {string} exchangeRate Exchange rate (hex) for the Pay By Token tx.
 * @param {number} decimals Decimals of the token for the Pay By Token tx.
 * @return {object} Token gas fee (in decimals).
 */
export function calculateTokenGasFee (gasLimit, gasPrice, exchangeRate, decimals) {
  return removeTrailingZeros(new BigNumber(gasLimit, 16)
    .mul(gasPrice, 16)
    .div(new BigNumber(exchangeRate, 16))
    .toFormat(decimals))
}
