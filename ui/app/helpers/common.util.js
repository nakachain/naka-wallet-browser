import { isString } from 'lodash'

export function camelCaseToCapitalize (str = '') {
  return str
    .replace(/([A-Z])/g, ' $1')
    .replace(/^./, str => str.toUpperCase())
}

export function isNonEmptyString (str) {
  return isString(str) && str.length > 0
}
