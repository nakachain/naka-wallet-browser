import currencyFormatter from 'currency-formatter'
import currencies from 'currency-formatter/currencies'
import ethUtil from 'ethereumjs-util'
import BigNumber from 'bignumber.js'
import { find } from 'lodash'

import {
  conversionUtil,
  addCurrencies,
  multiplyCurrencies,
  conversionGreaterThan,
} from '../../conversion-util'
import { isPayByToken } from '../transactions.util'
import {
  unconfirmedTransactionsCountSelector,
} from '../../selectors/confirm-transaction'
import { NAKA } from '../../constants/common'
import { isNonEmptyString } from '../../helpers/common.util'
import { removeTrailingZeros } from '../../helpers/conversions.util'

export function increaseLastGasPrice (lastGasPrice) {
  return ethUtil.addHexPrefix(multiplyCurrencies(lastGasPrice, 1.1, {
    multiplicandBase: 16,
    multiplierBase: 10,
    toNumericBase: 'hex',
  }))
}

export function hexGreaterThan (a, b) {
  return conversionGreaterThan(
    { value: a, fromNumericBase: 'hex' },
    { value: b, fromNumericBase: 'hex' },
  )
}

export function getHexGasTotal ({ gasLimit, gasPrice }) {
  return ethUtil.addHexPrefix(multiplyCurrencies(gasLimit, gasPrice, {
    toNumericBase: 'hex',
    multiplicandBase: 16,
    multiplierBase: 16,
  }))
}

export function addEth (...args) {
  return args.reduce((acc, base) => {
    return addCurrencies(acc, base, {
      toNumericBase: 'dec',
      numberOfDecimals: 6,
    })
  })
}

export function addFiat (...args) {
  return args.reduce((acc, base) => {
    return addCurrencies(acc, base, {
      toNumericBase: 'dec',
      numberOfDecimals: 2,
    })
  })
}

export function getValueFromWeiHex ({
  value,
  fromCurrency = 'NAKA',
  toCurrency,
  conversionRate,
  numberOfDecimals,
  toDenomination,
}) {
  return conversionUtil(value, {
    fromNumericBase: 'hex',
    toNumericBase: 'dec',
    fromCurrency,
    toCurrency,
    numberOfDecimals,
    fromDenomination: 'WEI',
    toDenomination,
    conversionRate,
  })
}

export function getTransactionFee ({
  value,
  fromCurrency = 'NAKA',
  toCurrency,
  conversionRate,
  numberOfDecimals,
}) {
  return conversionUtil(value, {
    fromNumericBase: 'BN',
    toNumericBase: 'dec',
    fromDenomination: 'WEI',
    fromCurrency,
    toCurrency,
    numberOfDecimals,
    conversionRate,
  })
}

export function formatCurrency (value, currencyCode) {
  const upperCaseCurrencyCode = currencyCode.toUpperCase()

  return currencies.find(currency => currency.code === upperCaseCurrencyCode)
    ? currencyFormatter.format(Number(value), { code: upperCaseCurrencyCode, style: 'currency' })
    : value
}

export function convertTokenToFiat ({
  value,
  fromCurrency = 'NAKA',
  toCurrency,
  conversionRate,
  contractExchangeRate,
}) {
  const totalExchangeRate = conversionRate * contractExchangeRate

  return conversionUtil(value, {
    fromNumericBase: 'dec',
    toNumericBase: 'dec',
    fromCurrency,
    toCurrency,
    numberOfDecimals: 2,
    conversionRate: totalExchangeRate,
  })
}

export function hasUnconfirmedTransactions (state) {
  return unconfirmedTransactionsCountSelector(state) > 0
}

export function roundExponential (value) {
  const PRECISION = 4
  const bigNumberValue = new BigNumber(String(value))

  // In JS, numbers with exponentials greater than 20 get displayed as an exponential.
  return bigNumberValue.e > 20 ? Number(bigNumberValue.toPrecision(PRECISION)) : value
}

/**
 * Handles all cases for the different sends:
 * 1. Send NAKA, pay with NAKA
 * 2. Send NAKA, pay with Token
 * 3. Send Token, pay with NAKA
 * 4. Send Token, pay with same Token
 * 5. Send Token, pay with different Token
 *
 * @param {object} props Props of the component.
 * @return {string} Text to display for the Confirm page Total.
 */
export function getConfirmPageTotalText (props) {
  const {
    ethTransactionAmount,
    hexTransactionFee,
    hexTransactionTotal,
    tokenData,
    tokenProps,
    selectedTokenAddress,
    payByTokenProps,
  } = props

  const isTransfer = () => {
    return tokenData &&
      tokenData.name === 'transfer' &&
      tokenProps &&
      isNonEmptyString(tokenProps.tokenSymbol)
  }
  const formatGasFee = (fee) => getValueFromWeiHex({
    value: fee,
    numberOfDecimals: 6,
  })

  if (isPayByToken(payByTokenProps)) {
    const {
      tokenAddress: pbtTokenAddress,
      tokenSymbol: pbtTokenSymbol,
      tokenDecimals: pbtTokenDecimals,
      tokenTransactionFeeDec,
      tokenTransactionFeeHex,
    } = payByTokenProps

    if (isTransfer()) {
      // Send Token, Pay With Token
      const tokenValue = find(tokenData.params, { name: '_value' })
      if (tokenValue) {
        if (selectedTokenAddress === pbtTokenAddress) {
          // Sending with and paying with same token. Combine values.
          const combinedFee = removeTrailingZeros(
            new BigNumber(tokenValue.value)
            .add(new BigNumber(tokenTransactionFeeHex, 16))
            .div(new BigNumber(10).pow(Number(pbtTokenDecimals)))
            .toFormat(6))
          return `${combinedFee} ${pbtTokenSymbol}`
        } else {
          // Sending Token A and paying with Token B
          const sendAmt = removeTrailingZeros(
            new BigNumber(tokenValue.value)
            .div(new BigNumber(10).pow(Number(tokenProps.tokenDecimals)))
            .toFormat(6))
          const formattedPbtAmt = removeTrailingZeros(
            new BigNumber(tokenTransactionFeeDec)
            .toFormat(6))
          return `${sendAmt} ${tokenProps.tokenSymbol} + ${formattedPbtAmt} ${pbtTokenSymbol}`
        }
      }
    } else {
      // Send NAKA, pay with Token
      return `${ethTransactionAmount} ${NAKA} + ${tokenTransactionFeeDec} ${pbtTokenSymbol}`
    }
  } else {
    if (isTransfer()) {
      // Send Token, pay with NAKA
      const tokenAmt = new BigNumber(tokenData.params[1].value, 10)
        .div(new BigNumber(10).pow(tokenProps.tokenDecimals))
      const nativeTxFee = formatGasFee(hexTransactionFee)
      return `${tokenAmt} ${tokenProps.tokenSymbol} + ${nativeTxFee} ${NAKA}`
    } else {
      // Send NAKA, pay with NAKA
      const txFeeTotal = formatGasFee(hexTransactionTotal)
      return `${txFeeTotal} ${NAKA}`
    }
  }
  return ''
}
