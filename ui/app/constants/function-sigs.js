export default {
  '0x2bdb7097': {
    name: 'setRate',
    params: ['address', 'uint256'],
  },
  '0x318f91d3': {
    name: 'assignName',
    params: ['string'],
  },
  '0xa9059cbb': {
    name: 'transfer',
    params: ['address', 'uint256'],
  },
  '0xbe45fd62': {
    name: 'transfer',
    params: ['address', 'uint256', 'bytes'],
  },
  '0xf2fde38b': {
    name: 'transferOwnership',
    params: ['address'],
  },
}
