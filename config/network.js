const NAKABASE_URL = 'https://base.nakachain.org'

const MAINNET = {
  id: 2019,
  name: 'mainnet',
  displayName: 'Mainnet',
  explorerHost: 'https://explorer.nakachain.org',
  explorerApiHost: `${NAKABASE_URL}/mainnet/explorer?apikey=${process.env.NAKABASE_API_KEY}`,
  web3Host: `${NAKABASE_URL}/mainnet/web3/${process.env.NAKABASE_API_KEY}`,
}
const TESTNET = {
  id: 2018,
  name: 'testnet',
  displayName: 'Testnet',
  explorerHost: 'https://testnet.explorer.nakachain.org',
  explorerApiHost: `${NAKABASE_URL}/testnet/explorer?apikey=${process.env.NAKABASE_API_KEY}`,
  web3Host: `${NAKABASE_URL}/testnet/web3/${process.env.NAKABASE_API_KEY}`,
}
const NETWORKS = [MAINNET, TESTNET]

const getNetworkById = (networkId) => {
  return NETWORKS.find(network => {
    return network.id === Number(networkId)
  })
}

const getNetworkByName = (networkName) => {
  return NETWORKS.find(network => {
    return network.name === networkName
  })
}

module.exports = {
  MAINNET,
  TESTNET,
  getNetworkById,
  getNetworkByName,
}
