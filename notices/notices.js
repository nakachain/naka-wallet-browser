// fs.readFileSync is inlined by browserify transform "brfs"
const fs = require('fs')
const path = require('path')

module.exports = [
  {
    id: 0,
    read: false,
    date: 'Wed Dec 19 2018',
    title: 'Terms of Use',
    body: fs.readFileSync(path.join(__dirname, '/archive', 'notice_0.md'), 'utf8'),
  },
  {
    id: 2,
    read: false,
    date: 'Mon May 08 2017',
    title: 'Privacy Notice',
    body: fs.readFileSync(path.join(__dirname, '/archive', 'notice_1.md'), 'utf8'),
  },
]
