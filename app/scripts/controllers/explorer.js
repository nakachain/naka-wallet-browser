const EventEmitter = require('safe-event-emitter')
const ObservableStore = require('obs-store')
const axios = require('axios')
const log = require('loglevel')
const { MAINNET, TESTNET } = require('../../../config/network')
const txUtils = require('./transactions/lib/util')

const NETWORK = { mainnet: MAINNET, testnet: TESTNET }

class ExplorerController extends EventEmitter {
  constructor (opts) {
    super()
    this.setExplorerTxs = opts.setExplorerTxs
    this.networkController = opts.networkController
    this.networkStore = this.networkController.networkStore || new ObservableStore({})
    this.preferencesStore = opts.preferencesStore || new ObservableStore({})
    this._mapMethods()

    this.pageSize = 300
  }

  setBaseUrl ({ type }) {
    if (NETWORK[type]) {
      this.baseUrl = NETWORK[type].explorerApiHost
    }
  }

  async tokentx (selectedTokenAddress) {
    if (!this.baseUrl) return { result: [] }
    try {
      const accountAddr = this.getSelectedAddress()
      const { data } = await axios.get(
        `${this.baseUrl}` +
        '&module=account' +
        '&action=tokentx' +
        `&address=${accountAddr}` +
        `&contractaddress=${selectedTokenAddress}`
      )
      const convertedTxs = data.result.map((tx) => txUtils.convertExplorerTrans(tx, this.getNetwork()))
      this.setExplorerTxs(convertedTxs)
    } catch (error) {
      log.error(`error ExplorerController.tokentx: ${error}`)
    }
  }

  async txlist () {
    if (!this.baseUrl) return { result: [] }

    try {
      // TODO: add pagination to tx list
      const accountAddr = this.getSelectedAddress()
      const { data } = await axios.get(
        `${this.baseUrl}` +
        '&module=account' +
        '&action=txlist' +
        `&address=${accountAddr}` +
        `&exchanger=${accountAddr}` +
        `&offset=${this.pageSize}` +
        '&page=1' +
        '&sort=desc'
      )
      const convertedTxs = data.result.map((tx) => txUtils.convertExplorerTrans(tx, this.getNetwork(), true))
      this.setExplorerTxs(convertedTxs)
    } catch (error) {
      log.error(`error ExplorerController.txlist: ${error}`)
    }
  }

  // maps methods for convenience
  _mapMethods () {
    /** @returns the network number stored in networkStore */
    this.getNetwork = () => this.networkStore.getState()
    /** @returns the user selected address */
    this.getSelectedAddress = () => this.preferencesStore.getState().selectedAddress
  }
}

module.exports = ExplorerController
