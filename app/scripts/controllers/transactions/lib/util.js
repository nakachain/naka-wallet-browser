const {
  addHexPrefix,
  isValidAddress,
} = require('ethereumjs-util')
const { decimalToHex } = require('../../../../../ui/app/helpers/conversions.util')

/**
@module
*/
module.exports = {
  normalizeTxParams,
  validateTxParams,
  validateFrom,
  validateRecipient,
  getFinalStates,
  convertExplorerTrans,
}


// functions that handle normalizing of that key in txParams
const normalizers = {
  from: from => addHexPrefix(from).toLowerCase(),
  to: to => addHexPrefix(to).toLowerCase(),
  nonce: nonce => addHexPrefix(nonce),
  value: value => addHexPrefix(value),
  data: data => addHexPrefix(data),
  gas: gas => addHexPrefix(gas),
  gasPrice: gasPrice => addHexPrefix(gasPrice),
  token: token => addHexPrefix(token),
  exchangeRate: exchangeRate => addHexPrefix(exchangeRate),
  exchanger: exchanger => addHexPrefix(exchanger),
}

const txParamKeys = [
  'from',
  'to',
  'nonce',
  'value',
  'data',
  'gas',
  'gasPrice',
  'token',
  'exchangeRate',
  'exchanger',
  'payByToken',
]

 /**
  map transactions from explorer api to correct structure
  @param trans {object}
  @param networkId {string}
  @returns {object} mapped txParams
 */
function convertExplorerTrans (trans, networkId = '2019', isNaka = false) {
  let txParams = {}
  txParamKeys.forEach((key) => {
    if (key in trans) {
      if (['nonce', 'value', 'exchangeRate', 'gas', 'gasPrice'].includes(key)) {
        txParams = {...txParams, ...{[key]: addHexPrefix(decimalToHex(trans[key])).toLowerCase()}}
      } else {
        txParams = {...txParams, ...{[key]: trans[key]}}
      }
    }
  })
  txParams.data = trans.input
  txParams.toUser = txParams.to
  txParams.to = trans.contractAddress
  const txReceipt = {}
  txReceipt.gasUsed = addHexPrefix(decimalToHex(trans.gasUsed)).toLowerCase()
  const converted = {}
  converted.time = parseInt(trans.timeStamp) * 1000
  converted.hash = trans.hash
  converted.status = 'confirmed'
  converted.txReceipt = txReceipt
  converted.metamaskNetworkId = networkId
  converted.tokenTransfers = trans.tokenTransfers || []
  converted.setRateToken = trans.setRateToken || null

  if (isNaka) {
    txParams.toUser = null
    const tx = converted.tokenTransfers.filter(transfer => transfer.payByToken === false)
    if (trans.to.length === 0 && tx.length !== 0) {
      // contract deployment
      txParams.to = null
    } else if (tx.length !== 0) {
      txParams.to = tx[0].to
    } else {
      txParams.to = trans.to
    }
  }
  converted.txParams = txParams
  return converted
}

 /**
  normalizes txParams
  @param txParams {object}
  @returns {object} normalized txParams
 */
function normalizeTxParams (txParams) {
  // apply only keys in the normalizers
  const normalizedTxParams = {}
  for (const key in normalizers) {
    if (txParams[key]) normalizedTxParams[key] = normalizers[key](txParams[key])
  }
  return normalizedTxParams
}

 /**
  validates txParams
  @param txParams {object}
 */
function validateTxParams (txParams) {
  validateFrom(txParams)
  validateRecipient(txParams)
  if ('value' in txParams) {
    const value = txParams.value.toString()
    if (value.includes('-')) {
      throw new Error(`Invalid transaction value of ${txParams.value} not a positive number.`)
    }

    if (value.includes('.')) {
      throw new Error(`Invalid transaction value of ${txParams.value} number must be in wei`)
    }
  }
}

 /**
  validates the from field in  txParams
  @param txParams {object}
 */
function validateFrom (txParams) {
  if (!(typeof txParams.from === 'string')) throw new Error(`Invalid from address ${txParams.from} not a string`)
  if (!isValidAddress(txParams.from)) throw new Error('Invalid from address')
}

 /**
  validates the to field in  txParams
  @param txParams {object}
 */
function validateRecipient (txParams) {
  if (txParams.to === '0x' || txParams.to === null) {
    if (txParams.data) {
      delete txParams.to
    } else {
      throw new Error('Invalid recipient address')
    }
  } else if (txParams.to !== undefined && !isValidAddress(txParams.to)) {
    throw new Error('Invalid recipient address')
  }
  return txParams
}

  /**
    @returns an {array} of states that can be considered final
  */
function getFinalStates () {
  return [
    'rejected', // the user has responded no!
    'confirmed', // the tx has been included in a block.
    'failed', // the tx failed for some reason, included on tx data.
    'dropped', // the tx nonce was already used
  ]
}

