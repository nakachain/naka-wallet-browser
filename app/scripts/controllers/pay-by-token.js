import ObservableStore from 'obs-store'
import Web3 from 'web3'
import { BN } from 'ethereumjs-util'

import NRC223 from '../../../config/contracts/nrc223'
import TokenExchange from '../../../config/contracts/token-exchange'

/**
 * A controller for all Pay By Token functionality.
 */
class PayByTokenController {

  constructor ({ network }) {
    this.store = new ObservableStore()
    this.network = network
    this.web3 = new Web3(network._provider)
  }

  async getExchangeRate (token) {
    try {
      // Get owner of token to check exchange rate
      const { address: tokenAddress, ...tokenInfo } = token
      const owner = await this._getContractOwner(NRC223.abi, tokenAddress)
      const exchRate = await this._getExchangerExchangeRate(tokenAddress, owner)
      if (exchRate.eq(0)) {
        throw Error('No exchange rate found')
      }

      return {
        tokenAddress,
        exchangerAddress: owner,
        rate: new BN(exchRate.toString(), 10),
        ...tokenInfo,
      }
    } catch (err) {
      return undefined
    }
  }

  async getExchangeRates (tokens, cb) {
    const tokenArray = Array.isArray(tokens) ? tokens : [tokens]
    const exchRates = {}
    let counter = 0
    tokenArray.forEach(async (token) => {
      const exchangeRate = await this.getExchangeRate(token)
      if (exchangeRate) {
        exchRates[token.address] = exchangeRate
      }

      // Execute callback to update rates once finished
      counter += 1
      if (counter === tokenArray.length) {
        cb(exchRates)
      }
    })
  }

  _getContractOwner (abi, address) {
    return new Promise((resolve, reject) => {
      const contract = this.web3.eth.contract(abi).at(address)
      contract.owner((err, owner) => {
        if (err) {
          reject(err)
          return
        }
        resolve(owner)
      })
    })
  }

  _getExchangerExchangeRate (tokenAddr, exchangerAddr) {
    return new Promise((resolve, reject) => {
      const tokenExchange = this.web3.eth.contract(TokenExchange.abi)
        .at(TokenExchange.address)
      tokenExchange.getRate(tokenAddr, exchangerAddr, (err, rate) => {
        if (err) {
          reject(err)
          return
        }
        resolve(rate)
      })
    })
  }
}

module.exports = PayByTokenController
